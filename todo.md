###
TODO

## Short term
* Abstract instance, device, physicaldevice, surface into context?
* Abstract graphics pipeline to hard code most stuff.

## Cool concepts to look into
* Pipeline cache
* swig?
* Vulkan Object naming.

## Long term / systems ideas
* Language backend, like Godot?
* Node based shader generator
