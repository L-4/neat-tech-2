# GNU make is hardly known for elegant solutions. Unless you find trapdoors and minefields to be elegant.
# - https://stackoverflow.com/a/38832692

OFLAGS := -c -MMD
CXXFLAGS := -I./src -std=c++20 -Wall -Wextra -Wpedantic -include "src/core/core.h"
BUILD_DIR := ./build
INTERMEDIATE_DIR := ./intermediate
SRC_DIR := ./src
EXECUTEABLE_BASENAME := neat_tech_2

VALID_COMPILERS = clang++ g++

GLSLC := glslc

debug ?= true

#   ______ _               _____  _____
#  |  ____| |        /\   / ____|/ ____|
#  | |__  | |       /  \ | |  __| (___
#  |  __| | |      / /\ \| | |_ |\___ \
#  | |    | |____ / ____ \ |__| |____) |
#  |_|    |______/_/    \_\_____|_____/

# A hack, for now: since this is the first ifeq, we assign BUILD_EXT as to leave out the first period.
ifeq ($(debug),true)
    CXXFLAGS += -DDEBUG -g -O0
    BUILD_EXT = debug
else ifeq ($(debug),false)
    CXXFLAGS += -O3
    BUILD_EXT =opt
else
    $(error You need to pass debug=true/false)
endif

compiler ?= clang++

ifneq ($(filter $(compiler),$(VALID_COMPILERS)),)
	BUILD_EXT := $(addsuffix .$(compiler),$(BUILD_EXT))
	CXX = $(compiler)
else
    $(error Invalid compiler. compiler must equal one of: $(VALID_COMPILERS))
endif

ifeq ($(compiler),g++)
    # Expection for this compiler/warning combination. See core/warnings.h
    CXXFLAGS += -Wno-unknown-pragmas
endif

editor ?= false

ifeq ($(editor),true)
    CXXFLAGS += -DEDITOR
    BUILD_EXT := $(addsuffix .editor,$(BUILD_EXT))
else ifneq ($(editor),false)
    $(error You need to pass editor=true/false)
endif

unity ?= false

ifeq ($(unity),true)
    BUILD_EXT := $(addsuffix .unity,$(BUILD_EXT))
else ifneq ($(unity),false)
    $(error You need to pass unity=true/false)
endif

#   _      _____ ____  _____            _____  _____ ______  _____
#  | |    |_   _|  _ \|  __ \     /\   |  __ \|_   _|  ____|/ ____|
#  | |      | | | |_) | |__) |   /  \  | |__) | | | | |__  | (___
#  | |      | | |  _ <|  _  /   / /\ \ |  _  /  | | |  __|  \___ \
#  | |____ _| |_| |_) | | \ \  / ____ \| | \ \ _| |_| |____ ____) |
#  |______|_____|____/|_|  \_\/_/    \_\_|  \_\_____|______|_____/

# Try to detect OS and choose libraries accordingly.
ifeq ($(OS),)
    OS = $(shell uname)
endif

# LDFLAGS = -lassimp
LDFLAGS = -lvulkan

ifeq ($(OS), Linux)
    # x11 is a bad name
    BUILD_EXT := $(addsuffix .x11,$(BUILD_EXT))
else ifeq ($(OS), Windows_NT)
    LDFLAGS += -lmingw32 -lSDL2main
    BUILD_EXT := $(addsuffix .win,$(BUILD_EXT))
else
    $(error Unable to detect or unknown OS (check `uname`))
endif

# -lSDL has to come after -lSDL2main. Thank you for being such a dear friend.
LDFLAGS += -lSDL2

#    _____       _______ _    _ ______ _____           _____ _____   _____
#   / ____|   /\|__   __| |  | |  ____|  __ \         / ____|  __ \ / ____|
#  | |  __   /  \  | |  | |__| | |__  | |__) |       | (___ | |__) | |
#  | | |_ | / /\ \ | |  |  __  |  __| |  _  /         \___ \|  _  /| |
#  | |__| |/ ____ \| |  | |  | | |____| | \ \         ____) | | \ \| |____
#   \_____/_/    \_\_|  |_|  |_|______|_|  \_\       |_____/|_|  \_\\_____|

# Now we can generate a list of target files.

# cpp build
CPP_FILES := $(shell find src -type f -name "*.cpp" | cut -d '/' -f2-)
OBJ_FILES := $(patsubst %.cpp,$(INTERMEDIATE_DIR)/%.$(BUILD_EXT).o,$(CPP_FILES))
D_FILES := $(patsubst %.cpp,$(INTERMEDIATE_DIR)/%.$(BUILD_EXT).d,$(CPP_FILES))

# shader build
FRAG_FILES := $(shell find shaders_src/frag -type f -name "*.glsl" | cut -d '/' -f3-)
VERT_FILES := $(shell find shaders_src/vert -type f -name "*.glsl" | cut -d '/' -f3-)
GLSL_FILES := $(FRAG_FILES) $(VERT_FILES)
SPV_FILES := $(patsubst %.glsl,shaders/%.spv,$(GLSL_FILES))

unity:
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $(shell find src/ - -name "*.cpp") -o $(BUILD_DIR)/neat_tech_2.$(BUILD_EXT).unity

# Try to detect cores.
# -NUM_CORES = $(shell nproc)
# ifeq ($(NUM_CORES),)
#     $(warning Could not detect number of cores - assuming one.)
#     NUM_CORES = 1
# endif

-include $(D_FILES)

# Build for cpp

$(INTERMEDIATE_DIR)/%.$(BUILD_EXT).o: $(SRC_DIR)/%.cpp
	@mkdir -p $(shell dirname $@)
	$(CXX) $(OFLAGS) $(CXXFLAGS) $< -o $@

# Build for spv

shaders/%.spv: shaders_src/frag/%.glsl
	@mkdir -p $(shell dirname $@)
	$(GLSLC) -fshader-stage=frag $< -o $@
shaders/%.spv: shaders_src/vert/%.glsl
	@mkdir -p $(shell dirname $@)
	$(GLSLC) -fshader-stage=vert $< -o $@

# Build for application

$(BUILD_DIR)/neat_tech_2.$(BUILD_EXT): $(OBJ_FILES) $(SPV_FILES)
	@mkdir -p $(BUILD_DIR) $(INTERMEDIATE_DIR)
	$(CXX) $(CXXFLAGS) $(OBJ_FILES) $(LDFLAGS) -o $(BUILD_DIR)/neat_tech_2.$(BUILD_EXT)

clean:
	rm -rf $(BUILD_DIR)/* $(INTERMEDIATE_DIR)/* shaders/*

build: $(BUILD_DIR)/$(EXECUTEABLE_BASENAME).$(BUILD_EXT)

.DEFAULT_GOAL = build

.PHONY: unity
