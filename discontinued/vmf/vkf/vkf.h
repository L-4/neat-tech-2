#pragma once

#include "containers/Array.h"
#include "containers/Pair.h"
#include "containers/StringSlice.h"
#include "util/ref.h"

namespace vkf
{

enum class ETokenType
{
	/** This is what it is called in the docs. Just a key but for a block. */
	Class,
	Key,
	Value,
	OpenBracket,
	CloseBracket,
	/** This is for when we either have a class or a key. */
	UnknownKeyType,
};

struct Token
{
	ETokenType m_type;
	StringSlice m_data;

	Token(ETokenType type, StringSlice data) : m_type(type), m_data(data) {}
};

struct Element
{
	Array<Pair<StringSlice, StringSlice>> m_properties;
	Array<Pair<StringSlice, Element>> m_children;
};

/**
 * A vkf file's tokens, with some helper methods to traverse it.
 * Called "simple" as there might be a need for an "advanced" version later
 * implemented with hash maps.
 */
struct VkfFileSimple
{
	/**
	 * File data which tokens point at.
	 * This is technically larger than it has to be, but this struct isn't meant
	 * to live for long anyway.
	 */
	Ref<char> m_fileData;
	StringSlice m_fileSlice;
	Array<Token> m_tokens;

	Element m_rootElement;

	void Tokenize();

	VkfFileSimple(const char* filename);

	void Print(const Element& element, u32 depth = 0) const;
};

} // namespace vkf
