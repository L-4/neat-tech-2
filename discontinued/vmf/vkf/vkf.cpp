#include "vkf.h"

#include "util/asserts.h"
#include "util/char.h"
#include <SDL2/SDL_rwops.h>

namespace vkf
{

/** Helper methods to parse vmf specific tokens. */
inline bool IsVmfIdentifierFirstChar(char c)
{
	return Char::IsAlphabetic(c) || c == '_' || c == '$';
}

inline bool IsVmfIdentifier(char c)
{
	return Char::IsAlphanumeric(c) || c == '_' || c == '$';
}

void VkfFileSimple::Tokenize()
{
	StringMarcher marcher(m_fileSlice);
	bool lastWasKey = false;

	while (!marcher.IsEof())
	{
		char c = marcher.GetChar();

		if (IsVmfIdentifierFirstChar(c) || Char::IsQuote(c))
		{
			// This is a bit convoluted: isUnquoted == IsVmfIdentifierFirstChar(),
			// but we know that this is also the case if !Char::IsQuite(c).
			// Therefore we use this call as it is considerably cheaper, and
			// this is in a hot loop.
			bool isUnquoted = !Char::IsQuote(c);

			// This is only used if we are goin into the quote branch, but we need
			// the char before we increment the marcher.
			char quoteType = c;

			// We don't want the slice to contain the char itself.
			if (!isUnquoted) marcher.GetChar();

			StringSlice stringSlice = marcher.BeginSlice();

			if (isUnquoted)
			{
				// Unquoted literal.
				/** First call to IsEof() is redundant. */
				/** @todo: Move to MarchIdentifier or the like. */
				while (!marcher.IsEof() && IsVmfIdentifier(marcher.GetChar())) ++stringSlice;

				// We know that we've eaten one char too many.
				marcher.GoBack();
			}
			else
			{
				while (!marcher.IsEof() && marcher.GetChar() != quoteType) ++stringSlice;
			}

			if (lastWasKey)
			{
				// We know that this is now a value.
				Token& lastToken = m_tokens.Last();

				// We know that we didn't know what type the token was.
				ASSERT(lastToken.m_type == ETokenType::UnknownKeyType);
				lastToken.m_type = ETokenType::Key;

				m_tokens.Emplace(ETokenType::Value, stringSlice);
			}
			else
			{
				// Can be either a key or a class.
				m_tokens.Emplace(ETokenType::UnknownKeyType, stringSlice);
			}

			// In either case the state of lastWasKey has changed
			lastWasKey = !lastWasKey;
		}
		else if (c == '{')
		{
			// Last has to have been a key, otherwise what block are we opening?
			ASSERT(lastWasKey);

			// We know that this is now a class.
			Token& lastToken = m_tokens.Last();

			// We know that we didn't know what type the token was.
			ASSERT(lastToken.m_type == ETokenType::UnknownKeyType);
			lastToken.m_type = ETokenType::Class;

			lastWasKey = false;

			m_tokens.Emplace(ETokenType::OpenBracket, marcher.BeginSlice());
		}
		else if (c == '}')
		{
			ASSERT(!lastWasKey);
			m_tokens.Emplace(ETokenType::CloseBracket, marcher.BeginSlice());
		}
	}

	// for (const auto& token : m_tokens)
	// { printf("%d: %s\n", token.m_type, token.m_data.GetAsCString().m_data); }

	Array<Element*> stack;
	stack.Push(&m_rootElement);

	// Eat tokens.
	for (i32 i = 0; i < m_tokens.GetCount(); ++i)
	{
		Token& token = m_tokens[i];
		switch (token.m_type)
		{
			case ETokenType::Key:
			{
				Token& nextToken = m_tokens[++i];
				ASSERT(nextToken.m_type == ETokenType::Value);
				stack.Last()->m_properties.Emplace(token.m_data, nextToken.m_data);
				break;
			}
			case ETokenType::Class:
			{
				Token& nextToken = m_tokens[++i];
				ASSERT(nextToken.m_type == ETokenType::OpenBracket);
				Pair<StringSlice, Element>& newClass =
					stack.Last()->m_children.Emplace(token.m_data, Element());

				stack.Push(&newClass.m_value);
				break;
			}
			case ETokenType::CloseBracket:
			{
				ASSERT(stack.GetCount() >= 1);

				stack.Pop();
				break;
			}
			case ETokenType::Value:
			{
				UNREACHABLE();
				break;
			}
			default:
			{
				UNREACHABLE();
				break;
			}
		}
	}
}

VkfFileSimple::VkfFileSimple(const char* filename)
{
	SDL_RWops* handle = SDL_RWFromFile(filename, "r");
	ASSERT(handle);

	i32 fileLength = SDL_RWsize(handle);
	m_fileData	   = Ref<char>(new char[fileLength], true);
	SDL_RWread(handle, m_fileData.m_data, 1, fileLength);
	m_fileSlice = StringSlice(m_fileData.Ptr(), fileLength);

	SDL_RWclose(handle);

	Tokenize();
}

void VkfFileSimple::Print(const Element& element, u32 depth) const
{
	printf("len: %d\n", m_tokens.GetCount());
	const char* tabs = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
	for (const auto& property : element.m_properties)
	{
		// printf("%s.*%s = %s\n",
		// 	   tabs,
		// 	   property.m_key.GetAsCString().Ptr(),
		// 	   property.m_value.GetAsCString().Ptr());
	}

	for (const auto& child : element.m_children)
	{
		printf("%s\n", child.m_key);
		// Print(child.m_value);
		return;
	}
}

} // namespace vkf
