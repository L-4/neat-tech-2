#pragma once

#include "containers/StringSlice.h"

namespace vmf
{
enum class EVKFTokenType
{

};

struct VKFToken
{
	EVKFTokenType m_type;
	StringSlice m_data;

	VKFToken
};

} // namespace vmf
