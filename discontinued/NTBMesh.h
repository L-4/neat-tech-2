#pragma once

#include "renderer/opengl/Program.h"
#include "renderer/opengl/VertexArray.h"
#include "renderer/opengl/VertexBuffer.h"

#include "containers/Array.h"
#include <glm/vec3.hpp>

/** Helper mesh to render NTBs */
class NTBMesh
{
private:
	NTBMesh();

	Array<glm::vec3> m_positionsData;
	VertexBuffer m_positionsBuffer;
	Array<glm::vec3> m_normalsData;
	VertexBuffer m_normalsBuffer;
	Array<glm::vec3> m_colorsData;
	VertexBuffer m_colorsBuffer;

	u32 m_numUploadedLines = 0;
	u32 m_numLines		   = 0;

	f32 m_lineExtents = 1.0;

	VertexArray m_vertexArray;

	Program m_program;

public:
	void SetLineExtents(f32 NewExtent);
	f32 GetLineExtents() { return m_lineExtents; }

	enum class ENTBColor : u32
	{
		Normal,
		Tangent,
		Bitangent,
	};

	void ClearLines();
	void PushLine(const glm::vec3& origin, const glm::vec3& direction, ENTBColor color);
	void PushLine(const glm::vec3& origin, const glm::vec3& direction, const glm::vec3& color);

	/**
	 * @todo: This currently reallocates the VBOs on the GPU. It would be better to glBufferSubData.
	 * New generic buffer calls should be used.
	 */
	void CommitChanges();

	void Draw() const;
	const Program& GetProgram() const { return m_program; }

	static NTBMesh& GetInstance()
	{
		static NTBMesh NTBMeshInstance;
		return NTBMeshInstance;
	}
};
