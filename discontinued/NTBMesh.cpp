#include "NTBMesh.h"

#include <GL/glew.h>

NTBMesh::NTBMesh() : m_program("assets/ntb_shader.glsl")
{
	m_vertexArray.Bind();

	m_positionsBuffer.Bind();
	m_positionsBuffer.SetupAttribute(0, 3, GL_FLOAT);
	glVertexBindingDivisor(0, 1);
	// glVertexAttribDivisor(0, 1);
	m_normalsBuffer.Bind();
	m_normalsBuffer.SetupAttribute(1, 3, GL_FLOAT);
	glVertexBindingDivisor(1, 1);
	// glVertexAttribDivisor(1, 1);
	m_colorsBuffer.Bind();
	m_colorsBuffer.SetupAttribute(2, 3, GL_FLOAT);
	glVertexBindingDivisor(2, 1);
	// glVertexAttribDivisor(2, 1);

	m_vertexArray.Unbind();
}

void NTBMesh::SetLineExtents(f32 newExtents)
{
	m_lineExtents = newExtents;
	m_program.SetUniform("uLineExtents", newExtents);
}

void NTBMesh::ClearLines()
{
	m_numLines		   = 0;
	m_numUploadedLines = 0;

	m_positionsData.Clear();
	m_normalsData.Clear();
	m_colorsData.Clear();
}

void NTBMesh::PushLine(const glm::vec3& origin, const glm::vec3& direction, ENTBColor color)
{
	static glm::vec3 Pink(1.0, 0.0, 1.0);
	static glm::vec3 Cyan(0.0, 1.0, 1.0);
	static glm::vec3 Yellow(1.0, 1.0, 0.0);

	PushLine(origin,
			 direction,
			 color == ENTBColor::Normal ? Pink : color == ENTBColor::Tangent ? Cyan : Yellow);
}

void NTBMesh::PushLine(const glm::vec3& origin, const glm::vec3& direction, const glm::vec3& color)
{
	m_positionsData.Emplace(origin);
	m_normalsData.Emplace(direction);
	m_colorsData.Emplace(color);

	m_numLines++;
}

void NTBMesh::CommitChanges()
{
	m_positionsBuffer.Bind();
	m_positionsBuffer.UpdateData(m_positionsData.Sizeof(), m_positionsData.Ptr());
	m_normalsBuffer.Bind();
	m_normalsBuffer.UpdateData(m_normalsData.Sizeof(), m_normalsData.Ptr());
	m_colorsBuffer.Bind();
	m_colorsBuffer.UpdateData(m_colorsData.Sizeof(), m_colorsData.Ptr());

	m_numUploadedLines = m_numLines;
}

void NTBMesh::Draw() const
{
	m_vertexArray.Bind();
	m_program.Use();
	glDrawArraysInstanced(GL_LINES, 0, 2, m_numUploadedLines);
	// glDrawArrays(GL_TRIANGLES, 0, 2);
	m_vertexArray.Unbind();
}
