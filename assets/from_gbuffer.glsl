#ifdef VERTEX

in vec2 aVertexPosition;

out vec2 vVertexCoordinates;

void main()
{
    vVertexCoordinates = (aVertexPosition + 1.0) * 0.5;
    gl_Position = vec4(aVertexPosition, 0.0, 1.0);
}

#endif // VERTEX

#ifdef FRAGMENT

uniform int uNumActiveLights;

layout (shared) uniform ubLights {
    vec4 uLightPositions[16];
    vec4 uLightColors[16];
};

uniform sampler2D uAlbedoSpecular;
uniform sampler2D uWorldNormal;
uniform sampler2D uWorldPosition;

uniform float uSpecularHighlight = 200.0;
// uniform vec3 uAmbientColor;
uniform vec3 uCameraPosition;
uniform vec3 uLightDirection;

in vec2 vVertexCoordinates;

layout(location = 0) out vec4 outColor;

void main()
{
    vec4 albedoSpecularSample = texture(uAlbedoSpecular, vVertexCoordinates);
    vec3 worldNormalSample = normalize(texture(uWorldNormal, vVertexCoordinates).xyz * vec3(2.0) - vec3(1.0));
    vec3 worldPositionSample = texture(uWorldPosition, vVertexCoordinates).xyz;

    vec3 ambient = vec3(0.05);
    float specular_strength = albedoSpecularSample.a;
    vec3 viewDir = normalize(worldPositionSample - uCameraPosition);
    vec3 lightAffect = vec3(0.0);

    for (int lightIdx = 0; lightIdx < uNumActiveLights; ++lightIdx)
    {
        vec3 lightPosition = uLightPositions[lightIdx].xyz;
        float dist = distance(lightPosition, worldPositionSample);
        float attenuated = clamp(30.0 / dist, 0.0, 1.0);
        vec3 lightDirection = normalize(worldPositionSample - lightPosition);
        vec3 diffuse = vec3(max(dot(-lightDirection, worldNormalSample), 0.0f));
        vec3 reflectDir = normalize(reflect(-lightDirection, worldNormalSample));
        vec3 specular = vec3(pow(
            max(dot(viewDir, reflectDir), 0.0),
        uSpecularHighlight * specular_strength));
        vec3 lightColor = uLightColors[lightIdx].rgb;

        lightAffect += lightColor * (specular + diffuse);
    }

    // vec3 lightPosition = vec3(40.0, 40.0, 10.0);
    // float dist = distance(lightPosition, worldPositionSample);
    // float attenuated = clamp(30.0 / dist, 0.0, 1.0);
    // vec3 lightDirection = normalize(worldPositionSample - lightPosition);
    // vec3 diffuse = vec3(max(dot(-lightDirection, worldNormalSample), 0.0f));
    // vec3 reflectDir = normalize(reflect(-lightDirection, worldNormalSample));
    // vec3 specular = vec3(pow(
    //     max(dot(viewDir, reflectDir), 0.0), uSpecularHighlight * specular_strength)
    // );


    outColor = vec4(albedoSpecularSample.rgb * lightAffect + ambient, 1.0);
}

#endif // FRAGMENT
