#ifdef VERTEX

layout (std140) uniform ubMVP {
   mat4 uModel;
   mat4 uView;
   mat4 uProjection;
};

layout(location = 0) in vec3 aVertexPosition;
layout(location = 1) in vec3 aVertexNormal;
layout(location = 2) in vec3 aVertexColor;

uniform float uLineExtents = 1.0;

out vec3 vVertexColor;

void main()
{
    vec3 adjustedVertex = aVertexPosition + aVertexNormal * float(gl_VertexID) * uLineExtents;
    gl_Position = uProjection * uView * vec4(adjustedVertex, 1.0);

    vVertexColor = aVertexColor;
}

#endif // VERTEX

#ifdef FRAGMENT

in vec3 vVertexColor;

layout(location = 0) out vec4 outColor;

void main()
{
    outColor = vec4(vVertexColor, 1.0);
};

#endif // FRAGMENT
