#ifdef VERTEX

layout (std140) uniform ubMVP {
   mat4 uModel;
   mat4 uView;
   mat4 uProjection;
};

layout(location = 0) in vec3 aVertexPosition;
layout(location = 1) in vec2 aVertexCoordinates;
layout(location = 2) in vec3 aVertexNormal;
layout(location = 3) in vec3 aVertexTangent;

out vec2 vVertexCoordinates;
out vec3 vVertexWorldPosition;
out mat3 vTBN;

void main()
{
    vec3 T = normalize(vec3(uModel * vec4(aVertexTangent, 0.0)));
    vec3 N = normalize(vec3(uModel * vec4(aVertexNormal, 0.0)));
    // This is apparently good? @todo: look into this.
    // https://learnopengl.com/Advanced-Lighting/Normal-Mapping (bottom of the page.)
    T = normalize(T - dot(T, N) * N);
    vec3 B = cross(N, T);
    vTBN = mat3(T, B, N);

    vVertexCoordinates = aVertexCoordinates;
    vVertexWorldPosition = (uModel * vec4(aVertexPosition, 1.0)).xyz;

    mat4 MVP = uProjection * uView * uModel;
    gl_Position = MVP * vec4(aVertexPosition, 1.0);
}

#endif // VERTEX

#ifdef FRAGMENT

layout(binding = 0) uniform sampler2D uAlbedoSpecular;
layout(binding = 1) uniform sampler2D uNormal;

in vec2 vVertexCoordinates;
in vec3 vVertexWorldPosition;
in mat3 vTBN;

layout(location = 0) out vec4 outAlbedoSpecular;
layout(location = 1) out vec3 outWorldNormal;
layout(location = 2) out vec3 outWorldPosition;

void main()
{
    vec4 albedoSpecularSample = texture(uAlbedoSpecular, vVertexCoordinates);
    outAlbedoSpecular = albedoSpecularSample;

    vec3 normalSample = texture(uNormal, vVertexCoordinates).rgb * 2.0 - 1.0;
    outWorldNormal = (normalize(vTBN * normalSample) + 1.0) / 2.0;

    outWorldPosition = vVertexWorldPosition;
};

#endif // FRAGMENT
