#ifdef VERTEX

layout (std140) uniform ubMVP {
   mat4 uModel;
   mat4 uView;
   mat4 uProjection;
};

in vec3 aVertexPosition;
in vec3 aVertexColor;

out vec3 vVertexColor;

void main()
{
   mat4 MVP = uProjection * uView * uModel;
   vVertexColor = aVertexColor;
   gl_Position = MVP * vec4(aVertexPosition, 1.0);
}

#endif // VERTEX

#ifdef FRAGMENT

out vec4 outColor;
in vec3 vVertexColor;

void main()
{
   outColor = vec4(vVertexColor, 1.0);
};

#endif // FRAGMENT
