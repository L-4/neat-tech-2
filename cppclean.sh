#!/bin/bash

SOURCE_FILES=$(find src \
    -regextype posix-egrep \
    -type f -regex ".*\.(h|hpp|cpp)$" \
    -o -path "src/thirdparty" -prune -type f)

cppclean $SOURCE_FILES --include-path src/ --include-path /usr/include
