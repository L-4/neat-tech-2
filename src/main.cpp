/**
 * Engine:
 * 		Anything pertaining to the program execution itself. Exit status, versions,
 * 		main loop.
 * 		For now, for lack of a better idea, the engine will also be responsible for
 * 		ticks with delta time.
 *
 * Window:
 * 		SDL_Window handle
 *
 * InputManager:
 * 		Redistributes the events to the working class
 * 		can of worms: In SDL, events come from the window. We don't want this
 * 			to be in the window, as it then becomes complicated to determine
 * 			where we want to listen to events. Hence, the simplification:
 * 			InputManager is a global that can be called from anywhere (even
 * 			multiple windows). Might refactor later on.
 *
 * Renderer:
 *		State pertinent to rendering, such as GBuffer.

 * World:
 * 		Array<Node>
 *
 * Node:
 * 		Generic transformable, renderable object in world.
 * 		Does not know how to render self.
 *
 * Camera:
 * 		Struct with transform + fov.
 *
 * GBuffer:
 * 		Not specific to NT. Used in renderer.
 *
 * Editor:
 * 		Array<IPanel>
 */

#include "Engine.h"
#include "InputManager.h"
#include "Window.h"
#include "World.h"
#include "renderer/vk/VulkanRenderer.h"

// #include "bindings/Vmf.h"

// #include <sys/stat.h>

#include "globals.h"

i32 main(int /* argc */, char** /* argv */)
{
	// struct stat s;
	// stat("libs/main.so", &s);
	// printf("stat: %li\n", s.st_mtim.tv_nsec);

	// auto so = Vmf("libs/main.so");
	// so.foo(1234);

	// return 0;
	// Sets up global engine ptr.
	Engine::Initialize();

	// Sets up global input manager.
	InputManager::Initialize();

	g_world	 = new World;
	g_window = new Window(640, 480);
	VulkanRenderer renderer(*g_window);

	// @todo: This should come from something else.
	Camera camera(90.0f, 640, 480);

	// Here's where we'd load a savefile.

	return g_engine->BeginMainLoop([&]() {
		g_inputManager->PollEvents();

		renderer.Render(*g_world, camera);
	});
}
