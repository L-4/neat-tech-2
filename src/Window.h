#pragma once

#include <SDL2/SDL.h>

#include <vector>

/**
 * Window represents everything which is specific to a windowed application.
 * Ie. this wouldn't be included in a headless build.
 * That means that several SDL things might be required in headless builds, and therefore couldn't
 * go in here. This is not the case for now however.
 */
class Window
{
	u32 m_width;
	u32 m_height;

public:
	SDL_Window* m_sdlWindow;

	Window(u32 width, u32 height);
	~Window();

	u32 GetWidth() const { return m_width; }
	u32 GetHeight() const { return m_height; }

	void SetMouseGrab(bool newGrab);

	/**
	 * Returns whether the mouse is currently locked.
	 * @todo: This is not window specific.
	 */
	bool HasMouseLock() const;

	/** Get framebuffer size. May differ from m_width/m_height. */
	void GetFramebufferSize(i32* outWidth, i32* outHeight) const;

	/**
	 * Gets required extensions and pushes them into the array.
	 * It is written this way as to allow multiple sources to require extensions.
	 * @todo: Deal with duplicate extensions.
	 *
	 * @param outExtensions Array to fill with extensions.
	 */
	void GetRequiredExtensions(std::vector<const char*>& outExtensions) const;
};
