#pragma once

#include "containers/Array.h"
#include "core/Node.h"

class World
{
public:
	Array<Node*> m_nodes;

	template <typename TNodeType, typename... TArgs>
	void CreateNode(TArgs&&... args)
	{
		m_nodes.Emplace(new TNodeType(args...));
	}
};
