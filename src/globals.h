#pragma once

#include "Window.h"
#include "World.h"

// NON-STANDARD globals

extern Window* g_window;
extern World* g_world;
