#include "VulkanRenderer.h"

#include <SDL2/SDL_vulkan.h>

#include "core/File.h"

#include <array>
#include <set>
#include <utility>
#include <vector>

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

struct Vertex
{
	glm::vec3 m_pos;
	glm::vec3 m_color;
	glm::vec2 m_texCoord;
};

struct MVP
{
	glm::mat4 m_model;
	glm::mat4 m_view;
	glm::mat4 m_projection;
};

VulkanRenderer::VulkanRenderer(const Window& window)
	: m_context(window),
	  m_memoryAllocator(m_context)
{
	const std::vector<Vertex> vertices = {
		{ { -0.5f, -0.5f, 0.0f }, { 1.0f, 0.0f, 0.0f }, { 0.0f, 0.0f } },
		{ { 0.5f, -0.5f, 0.0f }, { 0.0f, 1.0f, 0.0f }, { 1.0f, 0.0f } },
		{ { 0.5f, 0.5f, 0.0f }, { 0.0f, 0.0f, 1.0f }, { 1.0f, 1.0f } },
		{ { -0.5f, 0.5f, 0.0f }, { 1.0f, 1.0f, 1.0f }, { 0.0f, 1.0f } },

		{ { -0.5f, -0.5f, -0.5f }, { 1.0f, 0.0f, 0.0f }, { 0.0f, 0.0f } },
		{ { 0.5f, -0.5f, -0.5f }, { 0.0f, 1.0f, 0.0f }, { 1.0f, 0.0f } },
		{ { 0.5f, 0.5f, -0.5f }, { 0.0f, 0.0f, 1.0f }, { 1.0f, 1.0f } },
		{ { -0.5f, 0.5f, -0.5f }, { 1.0f, 1.0f, 1.0f }, { 0.0f, 1.0f } }
	};

	const std::vector<u16> indices = { 0, 1, 2, 2, 3, 0, 4, 5, 6, 6, 7, 4 };

	vk::PhysicalDevice physicalDevice = m_context.m_physicalDevice;
	vk::Device device				  = m_context.m_device.get();
	vk::SurfaceKHR surface			  = m_context.m_surface.get();

	// @todo: Update all #pragma regions
	// @todo: Consistent naming for createInfos
#pragma region Create swapchain

	// Choose surface format
	const std::vector<vk::SurfaceFormatKHR> surfaceFormats =
		physicalDevice.getSurfaceFormatsKHR(surface);

	// We know that there is at least one.
	m_surfaceFormat = surfaceFormats[0];

	// Try to find a better format, if available. If not, fall back to any format.
	for (const vk::SurfaceFormatKHR& surfaceFormat : surfaceFormats)
	{
		if (surfaceFormat.format == vk::Format::eB8G8R8A8Srgb &&
			surfaceFormat.colorSpace == vk::ColorSpaceKHR::eSrgbNonlinear)
		{
			m_surfaceFormat = surfaceFormat;
			break;
		}
	}

	// Chose present mode
	// FIFO is guaranteed to exist.
	m_presentMode = vk::PresentModeKHR::eFifo;

	// Check if we can use vk::PresentModeKHR::eMailbox, if not, fallback to FIFO.
	for (const vk::PresentModeKHR presentMode : physicalDevice.getSurfacePresentModesKHR(surface))
	{
		if (presentMode == vk::PresentModeKHR::eMailbox)
		{
			m_presentMode = presentMode;
			break;
		}
	}

	// Choose swapchain extent
	const vk::SurfaceCapabilitiesKHR& surfaceCapabilities =
		physicalDevice.getSurfaceCapabilitiesKHR(surface);

	if (surfaceCapabilities.currentExtent.width != UINT32_MAX)
	{ m_swapChainExtent = surfaceCapabilities.currentExtent; }
	else
	{
		i32 fbWidth, fbHeight;
		window.GetFramebufferSize(&fbWidth, &fbHeight);

		m_swapChainExtent =
			vk::Extent2D { .width  = std::clamp(static_cast<u32>(fbWidth),
												surfaceCapabilities.minImageExtent.width,
												surfaceCapabilities.maxImageExtent.width),
						   .height = std::clamp(static_cast<u32>(fbWidth),
												surfaceCapabilities.minImageExtent.height,
												surfaceCapabilities.maxImageExtent.height) };
	}

	u32 swapChainImageCount = surfaceCapabilities.minImageCount;

	// if maxImageCount is zero, then there is no max limit.
	if (surfaceCapabilities.maxImageCount != 0 &&
		swapChainImageCount > surfaceCapabilities.maxImageCount)
		swapChainImageCount = surfaceCapabilities.maxImageCount;

	bool areAllQueueFamiliesSame = m_context.m_graphicsQueue == m_context.m_presentQueue;
	u32 queueFamilyIndices[] = { m_context.m_graphicsFamilyIndex, m_context.m_presentFamilyIndex };

	vk::SwapchainCreateInfoKHR swapChainCreateInfo {
		.surface		  = surface,
		.minImageCount	  = swapChainImageCount,
		.imageFormat	  = m_surfaceFormat.format,
		.imageColorSpace  = m_surfaceFormat.colorSpace,
		.imageExtent	  = m_swapChainExtent,
		.imageArrayLayers = 1,
		.imageUsage		  = vk::ImageUsageFlagBits::eColorAttachment,
		.imageSharingMode =
			areAllQueueFamiliesSame ? vk::SharingMode::eExclusive : vk::SharingMode::eConcurrent,
		.queueFamilyIndexCount = areAllQueueFamiliesSame ? 0u : 2u,
		.pQueueFamilyIndices   = areAllQueueFamiliesSame ? nullptr : queueFamilyIndices,
		.preTransform		   = surfaceCapabilities.currentTransform,
		.compositeAlpha		   = vk::CompositeAlphaFlagBitsKHR::eOpaque,
		.presentMode		   = m_presentMode,
		.clipped			   = true,
	};

	m_swapChain = device.createSwapchainKHRUnique(swapChainCreateInfo);

	m_swapChainImages = device.getSwapchainImagesKHR(m_swapChain.get());
	m_swapChainImageViews.reserve(m_swapChainImages.size());

	for (const vk::Image& image : m_swapChainImages)
	{
		vk::ImageViewCreateInfo imageViewCreateInfo {
			.image	  = image,
			.viewType	  = vk::ImageViewType::e2D,
			.format	  = m_surfaceFormat.format,
			.components = {
				.r = vk::ComponentSwizzle::eIdentity,
				.g = vk::ComponentSwizzle::eIdentity,
				.b = vk::ComponentSwizzle::eIdentity,
				.a = vk::ComponentSwizzle::eIdentity,
			},
			.subresourceRange = {
				.aspectMask = vk::ImageAspectFlagBits::eColor,
				.baseMipLevel = 0,
				.levelCount = 1,
				.baseArrayLayer = 0,
				.layerCount = 1
			}};

		m_swapChainImageViews.emplace_back(device.createImageViewUnique(imageViewCreateInfo));
	}

#pragma endregion

#pragma region Graphics pipeline
	{
#pragma region Shader stages
		File vertexShaderFile("shaders/vert.spv", "rb");
		std::vector<u8> vertexShader = vertexShaderFile.ReadWholeFile();
		// @todo: when we pass the shader code, it needs to be four byte aligned.
		// Our vector is in u8, but apparently std::vector will always pad allocations
		// to the next four bytes, but it would be nicer to assert that here.

		vk::ShaderModuleCreateInfo vertexShaderCreateInfo {
			.codeSize = vertexShader.size(),
			.pCode	  = reinterpret_cast<const u32*>(vertexShader.data()),
		};

		m_vertexShader = device.createShaderModuleUnique(vertexShaderCreateInfo);

		File fragmentShaderFile("shaders/frag.spv", "rb");
		std::vector<u8> fragmentShader = fragmentShaderFile.ReadWholeFile();
		// @todo: when we pass the shader code, it needs to be four byte aligned.
		// Our vector is in u8, but apparently std::vector will always pad allocations
		// to the next four bytes, but it would be nicer to assert that here.

		vk::ShaderModuleCreateInfo fragmentShaderCreateInfo {
			.codeSize = fragmentShader.size(),
			.pCode	  = reinterpret_cast<const u32*>(fragmentShader.data()),
		};

		m_fragmentShader = device.createShaderModuleUnique(fragmentShaderCreateInfo);

		vk::PipelineShaderStageCreateInfo shaderStages[] = {
			{
				.stage	= vk::ShaderStageFlagBits::eVertex,
				.module = m_vertexShader.get(),
				.pName	= "main",
			},
			{
				.stage	= vk::ShaderStageFlagBits::eFragment,
				.module = m_fragmentShader.get(),
				.pName	= "main",
			},
		};
#pragma endregion

#pragma region Vertex layout
		// This could be separated out.
		vk::VertexInputBindingDescription bindingDescription { .binding = 0,
															   .stride	= sizeof(Vertex),
															   .inputRate =
																   vk::VertexInputRate::eVertex };

		std::array<vk::VertexInputAttributeDescription, 3> attributeDescriptions = {
			vk::VertexInputAttributeDescription { .location = 0,
												  .binding	= 0,
												  .format	= vk::Format::eR32G32B32Sfloat,
												  .offset	= offsetof(Vertex, m_pos) },
			vk::VertexInputAttributeDescription { .location = 1,
												  .binding	= 0,
												  .format	= vk::Format::eR32G32B32Sfloat,
												  .offset	= offsetof(Vertex, m_color) },
			vk::VertexInputAttributeDescription { .location = 2,
												  .binding	= 0,
												  .format	= vk::Format::eR32G32Sfloat,
												  .offset	= offsetof(Vertex, m_texCoord) },
		};

		vk::PipelineVertexInputStateCreateInfo vertexInputInfo {
			.vertexBindingDescriptionCount	 = 1,
			.pVertexBindingDescriptions		 = &bindingDescription,
			.vertexAttributeDescriptionCount = attributeDescriptions.size(),
			.pVertexAttributeDescriptions	 = attributeDescriptions.data(),
		};
#pragma endregion

#pragma region Input assembly
		vk::PipelineInputAssemblyStateCreateInfo inputAssemblyInfo {
			.topology				= vk::PrimitiveTopology::eTriangleList,
			.primitiveRestartEnable = false,
		};
#pragma endregion

#pragma region Viewport state
		vk::Viewport viewport { .x		  = 0.0f,
								.y		  = 0.0f,
								.width	  = static_cast<float>(m_swapChainExtent.width),
								.height	  = static_cast<float>(m_swapChainExtent.height),
								.minDepth = 0.0f,
								.maxDepth = 0.0f };

		vk::Rect2D scissorRect { .offset = { 0, 0 }, .extent = m_swapChainExtent };

		vk::PipelineViewportStateCreateInfo viewportState { .viewportCount = 1,
															.pViewports	   = &viewport,
															.scissorCount  = 1,
															.pScissors	   = &scissorRect };
#pragma endregion

#pragma region Rasterization state
		vk::PipelineRasterizationStateCreateInfo rasterizationStateInfo {
			.depthClampEnable		 = false,
			.rasterizerDiscardEnable = false,
			.polygonMode			 = vk::PolygonMode::eFill,
			.cullMode				 = vk::CullModeFlagBits::eBack,
			.frontFace				 = vk::FrontFace::eCounterClockwise,
			.depthBiasEnable		 = false,
			.lineWidth				 = 1.0f,
		};
#pragma endregion

#pragma region Multisampling
		vk::PipelineMultisampleStateCreateInfo multisamplingInfo {
			.rasterizationSamples = vk::SampleCountFlagBits::e1,
			.sampleShadingEnable  = false,
		};
#pragma endregion

#pragma region Color blending
		vk::PipelineColorBlendAttachmentState colorBlendAttachment {
			.blendEnable	= false,
			.colorWriteMask = vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG |
							  vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA,
		};

		vk::PipelineColorBlendStateCreateInfo colorBlending {
			.logicOpEnable	 = false,
			.attachmentCount = 1,
			.pAttachments	 = &colorBlendAttachment,
		};
#pragma endregion

#pragma region Uniform layout
		{
			vk::DescriptorSetLayoutBinding uboLayoutBinding {
				.binding		 = 0,
				.descriptorType	 = vk::DescriptorType::eUniformBuffer,
				.descriptorCount = 1,
				.stageFlags		 = vk::ShaderStageFlagBits::eVertex,
			};

			vk::DescriptorSetLayoutBinding samplerLayoutBinding {
				.binding		 = 1,
				.descriptorType	 = vk::DescriptorType::eCombinedImageSampler,
				.descriptorCount = 1,
				.stageFlags		 = vk::ShaderStageFlagBits::eFragment,
			};

			std::array<vk::DescriptorSetLayoutBinding, 2> bindings = { uboLayoutBinding,
																	   samplerLayoutBinding };

			vk::DescriptorSetLayoutCreateInfo layoutInfo { .bindingCount = bindings.size(),
														   .pBindings	 = bindings.data() };

			m_descriptorSetLayout = device.createDescriptorSetLayoutUnique(layoutInfo);
		}
#pragma endregion

#pragma region Pipeline layout
		// Made local for reference.
		vk::DescriptorSetLayout descriptorSetLayout = m_descriptorSetLayout.get();
		vk::PipelineLayoutCreateInfo pipelineLayoutInfo { .setLayoutCount = 1,
														  .pSetLayouts	  = &descriptorSetLayout };

		m_pipelineLayout = device.createPipelineLayoutUnique(pipelineLayoutInfo);
#pragma endregion

#pragma region Render pass
		{
			vk::AttachmentDescription colorAttachment {
				.format			= m_surfaceFormat.format,
				.samples		= vk::SampleCountFlagBits::e1,
				.loadOp			= vk::AttachmentLoadOp::eClear,
				.storeOp		= vk::AttachmentStoreOp::eStore,
				.stencilLoadOp	= vk::AttachmentLoadOp::eDontCare,
				.stencilStoreOp = vk::AttachmentStoreOp::eDontCare,
				.initialLayout	= vk::ImageLayout::eUndefined,
				.finalLayout	= vk::ImageLayout::ePresentSrcKHR
			};

			vk::AttachmentReference colorAttachmentRef {
				.attachment = 0,
				.layout		= vk::ImageLayout::eColorAttachmentOptimal
			};

			vk::SubpassDescription subpass { .pipelineBindPoint = vk::PipelineBindPoint::eGraphics,
											 .colorAttachmentCount = 1,
											 .pColorAttachments	   = &colorAttachmentRef };

			vk::SubpassDependency subpassDependency {
				.srcSubpass	  = VK_SUBPASS_EXTERNAL,
				.dstSubpass	  = 0,
				.srcStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput,
				.dstStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput,
				// @todo: This is weird
				.srcAccessMask = static_cast<vk::AccessFlagBits>(0),
				.dstAccessMask = vk::AccessFlagBits::eColorAttachmentWrite,
			};

			vk::RenderPassCreateInfo renderPassInfo { .attachmentCount = 1,
													  .pAttachments	   = &colorAttachment,
													  .subpassCount	   = 1,
													  .pSubpasses	   = &subpass,
													  .dependencyCount = 1,
													  .pDependencies   = &subpassDependency };

			m_renderPass = device.createRenderPassUnique(renderPassInfo);
		}
#pragma endregion

#pragma region Construct pipeline
		vk::GraphicsPipelineCreateInfo pipelineInfo {
			.stageCount			 = 2,
			.pStages			 = shaderStages,
			.pVertexInputState	 = &vertexInputInfo,
			.pInputAssemblyState = &inputAssemblyInfo,
			.pTessellationState	 = nullptr,
			.pViewportState		 = &viewportState,
			.pRasterizationState = &rasterizationStateInfo,
			.pMultisampleState	 = &multisamplingInfo,
			.pDepthStencilState	 = nullptr,
			.pColorBlendState	 = &colorBlending,
			.pDynamicState		 = nullptr,
			.layout				 = m_pipelineLayout.get(),
			.renderPass			 = m_renderPass.get(),
			.subpass			 = 0,
		};

		// Null pipeline cache.
		m_graphicsPipeline =
			device.createGraphicsPipelineUnique(vk::PipelineCache(), pipelineInfo).value;
	}

#pragma endregion
#pragma endregion

#pragma region Framebuffers

	for (u32 imageIdx = 0; imageIdx < m_swapChainImages.size(); ++imageIdx)
	{
		vk::ImageView attachments[] = { m_swapChainImageViews[imageIdx].get() };

		vk::FramebufferCreateInfo framebufferInfo { .renderPass		 = m_renderPass.get(),
													.attachmentCount = 1,
													.pAttachments	 = attachments,
													.width			 = m_swapChainExtent.width,
													.height			 = m_swapChainExtent.height,
													.layers			 = 1 };

		m_swapchainFramebuffers.emplace_back(device.createFramebufferUnique(framebufferInfo));
	}

#pragma endregion

#pragma region Command buffers

	vk::CommandBufferAllocateInfo allocateInfo {
		.commandPool		= m_context.m_commandPool.get(),
		.level				= vk::CommandBufferLevel::ePrimary,
		.commandBufferCount = static_cast<u32>(m_swapchainFramebuffers.size()),
	};

	m_commandBuffers = device.allocateCommandBuffersUnique(allocateInfo);
#pragma endregion

#pragma region Create vertex buffers

	m_vertexBuffer = Buffer(m_context,
							m_memoryAllocator,
							vertices,
							vk::BufferUsageFlagBits::eVertexBuffer,
							vk::MemoryPropertyFlagBits::eDeviceLocal);

	m_indexBuffer = Buffer(m_context,
						   m_memoryAllocator,
						   indices,
						   vk::BufferUsageFlagBits::eIndexBuffer,
						   vk::MemoryPropertyFlagBits::eDeviceLocal);
#pragma endregion

#pragma region Create uniform buffers

	for (u32 i = 0; i < m_swapChainImages.size(); ++i)
	{
		// Use dataless overload.
		m_uniformBuffers.emplace_back(m_context,
									  m_memoryAllocator,
									  nullptr,
									  sizeof(MVP),
									  vk::BufferUsageFlagBits::eUniformBuffer,
									  vk::MemoryPropertyFlagBits::eHostVisible);
	}

#pragma endregion

#pragma region Create texture

	m_image		= Image(m_context, m_memoryAllocator, "assets/textures/AlbedoSpecular.png");
	m_imageView = m_image.CreateView();

	vk::SamplerCreateInfo samplerInfo {
		.magFilter				 = vk::Filter::eLinear,
		.minFilter				 = vk::Filter::eLinear,
		.mipmapMode				 = vk::SamplerMipmapMode::eLinear,
		.addressModeU			 = vk::SamplerAddressMode::eRepeat,
		.addressModeV			 = vk::SamplerAddressMode::eRepeat,
		.addressModeW			 = vk::SamplerAddressMode::eRepeat,
		.mipLodBias				 = 0.0f,
		.anisotropyEnable		 = false,
		.maxAnisotropy			 = 0.0f,
		.compareEnable			 = false,
		.compareOp				 = vk::CompareOp::eAlways,
		.minLod					 = 0.0f,
		.maxLod					 = 0.0f,
		.borderColor			 = vk::BorderColor::eIntOpaqueBlack,
		.unnormalizedCoordinates = false,
	};

	m_imageSampler = m_context.m_device->createSamplerUnique(samplerInfo);

#pragma endregion

#pragma region Create depth buffer

	m_depthImage = Image(m_context,
						 m_memoryAllocator,
						 vk::ImageUsageFlagBits::eDepthStencilAttachment,
						 vk::MemoryPropertyFlagBits::eDeviceLocal,
						 m_swapChainExtent.width,
						 m_swapChainExtent.height);

	m_depthImageView = m_depthImage.CreateView();

#pragma endregion

#pragma region Create descriptor pools
	std::array<vk::DescriptorPoolSize, 2> poolSize = {
		vk::DescriptorPoolSize { .type			  = vk::DescriptorType::eUniformBuffer,
								 .descriptorCount = static_cast<u32>(m_swapChainImages.size()) },
		vk::DescriptorPoolSize { .type			  = vk::DescriptorType::eCombinedImageSampler,
								 .descriptorCount = static_cast<u32>(m_swapChainImages.size()) },
	};

	vk::DescriptorPoolCreateInfo descriptorPoolInfo {
		.maxSets	   = static_cast<u32>(m_swapChainImages.size()),
		.poolSizeCount = poolSize.size(),
		.pPoolSizes	   = poolSize.data(),
	};

	m_descriptorPool = device.createDescriptorPoolUnique(descriptorPoolInfo);
#pragma endregion

#pragma region Create descriptor sets

	/** There a unique version of this, but apparently they are implicitly destroyed along with the
	 * pool. */
	std::vector<vk::DescriptorSetLayout> layouts(m_swapChainImages.size(),
												 m_descriptorSetLayout.get());

	vk::DescriptorSetAllocateInfo descriptorAllocInfo {
		.descriptorPool		= m_descriptorPool.get(),
		.descriptorSetCount = static_cast<u32>(m_swapChainImages.size()),
		.pSetLayouts		= layouts.data(),
	};

	/** These should also not be freed manually, unless
	 * DescriptorPoolCreateFlagBits::eFreeDescriptorSet is set. */
	m_descriptorSets = device.allocateDescriptorSets(descriptorAllocInfo);

	for (u32 i = 0; i < layouts.size(); ++i)
	{
		vk::DescriptorBufferInfo bufferInfo { .buffer = m_uniformBuffers[i].GetBuffer(),
											  .offset = 0,
											  .range  = sizeof(MVP) };

		vk::DescriptorImageInfo imageInfo {
			.sampler	 = m_imageSampler.get(),
			.imageView	 = m_imageView.get(),
			.imageLayout = vk::ImageLayout::eShaderReadOnlyOptimal,
		};

		std::array<vk::WriteDescriptorSet, 2> writeDescriptors {
			vk::WriteDescriptorSet { .dstSet		  = m_descriptorSets[i],
									 .dstBinding	  = 0,
									 .dstArrayElement = 0,
									 .descriptorCount = 1,
									 .descriptorType  = vk::DescriptorType::eUniformBuffer,
									 .pBufferInfo	  = &bufferInfo },
			vk::WriteDescriptorSet { .dstSet		  = m_descriptorSets[i],
									 .dstBinding	  = 1,
									 .dstArrayElement = 0,
									 .descriptorCount = 1,
									 .descriptorType  = vk::DescriptorType::eCombinedImageSampler,
									 .pImageInfo	  = &imageInfo },
		};

		device.updateDescriptorSets(writeDescriptors, nullptr);
	}

#pragma endregion

#pragma region Record render passes
	vk::ClearValue clearColor =
		vk::ClearColorValue(std::array<float, 4>({ { 0.0f, 0.0f, 0.0f, 1.0f } }));

	for (u32 cmdBufIdx = 0; cmdBufIdx < m_commandBuffers.size(); ++cmdBufIdx)
	{
		vk::CommandBufferBeginInfo beginInfo {};
		vk::UniqueCommandBuffer& commandBuffer = m_commandBuffers[cmdBufIdx];

		commandBuffer->begin(beginInfo);

		vk::RenderPassBeginInfo renderPassInfo {
			.renderPass = m_renderPass.get(),
			.framebuffer = m_swapchainFramebuffers[cmdBufIdx].get(),
			.renderArea = {
				.offset = { 0, 0 },
				.extent = m_swapChainExtent,
			},
			.clearValueCount = 1,
			.pClearValues = &clearColor};

		commandBuffer->beginRenderPass(renderPassInfo, vk::SubpassContents::eInline);

		commandBuffer->bindPipeline(vk::PipelineBindPoint::eGraphics, m_graphicsPipeline.get());

		/** Made local to have reference. */
		vk::DescriptorSet currentDescriptorSet = m_descriptorSets[cmdBufIdx];
		commandBuffer->bindDescriptorSets(vk::PipelineBindPoint::eGraphics,
										  m_pipelineLayout.get(),
										  0,
										  { currentDescriptorSet },
										  nullptr);

		commandBuffer->bindVertexBuffers(0, m_vertexBuffer.GetBuffer(), { 0 });
		commandBuffer->bindIndexBuffer(m_indexBuffer.GetBuffer(), 0, vk::IndexType::eUint16);
		commandBuffer->drawIndexed(static_cast<u32>(indices.size()), 1, 0, 0, 0);
		commandBuffer->endRenderPass();

		commandBuffer->end();
	}

#pragma endregion

#pragma region Create semaphores

	m_imagesInFlight.resize(m_swapChainImages.size());

	for (u32 frameIdx = 0; frameIdx < MAX_FRAMES_IN_FLIGHT; ++frameIdx)
	{
		m_imageAvailableSemaphores.emplace_back(device.createSemaphoreUnique({}));
		m_renderFinishedSemaphores.emplace_back(device.createSemaphoreUnique({}));
		m_inFlightFences.emplace_back(
			device.createFenceUnique({ .flags = vk::FenceCreateFlagBits::eSignaled }));
	}

#pragma endregion
}

void VulkanRenderer::UploadCamera(const Camera& camera, u32 imageIndex) const
{
	MVP mvp { .m_model		= glm::mat4(1.0),
			  .m_view		= camera.GetTransform().GetInvMatrix(),
			  .m_projection = camera.GetProjection() };

	mvp.m_projection[1][1] *= -1;

	m_uniformBuffers[imageIndex].UploadData(&mvp, sizeof(MVP));
}

void VulkanRenderer::Render(const World& /* world */, const Camera& camera)
{
	// Fence for frame being drawn.
	const vk::Fence& frameFence = m_inFlightFences[m_currentFrame].get();
	// Wait for the framebuffer needed to draw this frame to, to complete.
	ASSERT(m_context.m_device->waitForFences(frameFence, true, UINT64_MAX) == vk::Result::eSuccess);

	u32 imageIndex = m_context.m_device
						 ->acquireNextImageKHR(m_swapChain.get(),
											   UINT64_MAX,
											   m_imageAvailableSemaphores[m_currentFrame].get())
						 .value;

#ifdef TICK_TOCK
	auto start = m_clock.now();
#endif // TICK_TOCK

	if (m_imagesInFlight[imageIndex])
	{
		ASSERT((m_context.m_device->waitForFences(m_imagesInFlight[imageIndex], true, UINT64_MAX) ==
				vk::Result::eSuccess));
	}

	UploadCamera(camera, imageIndex);

	m_imagesInFlight[imageIndex] = frameFence;

	vk::Semaphore waitSemaphores[]	 = { m_imageAvailableSemaphores[m_currentFrame].get() };
	vk::Semaphore signalSemaphores[] = { m_renderFinishedSemaphores[m_currentFrame].get() };

	vk::PipelineStageFlags waitStages[] = { vk::PipelineStageFlagBits::eColorAttachmentOutput };

	vk::CommandBuffer commandBuffer = m_commandBuffers[imageIndex].get();
	vk::SubmitInfo submitInfo { .waitSemaphoreCount	  = 1,
								.pWaitSemaphores	  = waitSemaphores,
								.pWaitDstStageMask	  = waitStages,
								.commandBufferCount	  = 1,
								.pCommandBuffers	  = &commandBuffer,
								.signalSemaphoreCount = 1,
								.pSignalSemaphores	  = signalSemaphores };

	m_context.m_device->resetFences(frameFence);
	m_context.m_graphicsQueue.submit(submitInfo, frameFence);

	vk::SwapchainKHR swapChains[] = { m_swapChain.get() };

	vk::PresentInfoKHR presentInfo { .waitSemaphoreCount = 1,
									 .pWaitSemaphores	 = signalSemaphores,
									 .swapchainCount	 = 1,
									 .pSwapchains		 = swapChains,
									 .pImageIndices		 = &imageIndex };

	ASSERT(m_context.m_presentQueue.presentKHR(presentInfo) == vk::Result::eSuccess);

#ifdef TICK_TOCK
	auto end = m_clock.now();
	printf(
		"Frame time: %fus\n",
		std::chrono::duration_cast<std::chrono::duration<double, std::micro>>(end - start).count());
#endif // TICK_TOCK

	m_currentFrame = (m_currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
}

VulkanRenderer::~VulkanRenderer()
{
	// @todo: Move this to Context if everything else works.
	m_context.m_device->waitIdle();
}
