#pragma once

#include "renderer/vk/vulkan.hpp"

class GraphicsPipeline
{
	GraphicsPipeline(
		vk::Device device,
		/** Should be a vector. Refers to array of shader stages, vertex, fragment. */
		vk::PipelineShaderStageCreateInfo* shaderStages,
		/** Everything about vertices used:
		 Location and binding, offset, stride, vertex input rate (per vertex, per instance) */
		vk::PipelineVertexInputStateCreateInfo* vertexInputState,
		/** Draw mode (triangles, lines, points, triangle fan etc) and primitive restart enabled. */
		vk::PipelineInputAssemblyStateCreateInfo* inputAssemblyState,
		/** Information about tessellation. */
		vk::PipelineTessellationStateCreateInfo* tessellationState,
		/** List of viewport(s), scissor rects. */
		vk::PipelineViewportStateCreateInfo* viewportState,
		/** Cull mode, polygon facing, depth bias, some other properties. */
		vk::PipelineRasterizationStateCreateInfo* rasterizationState,
		/** Multisample count, properties. */
		vk::PipelineMultisampleStateCreateInfo* multisampleState,
		/** Depth and stencil buffer configuration. */
		vk::PipelineDepthStencilStateCreateInfo* depthStencilState,
		/** Everything to do with blending. glBlendFunc() */
		vk::PipelineColorBlendStateCreateInfo* colorBlendState,
		/** Contains array of identifiers telling the pipeline which properties can change
		   dynamically. */
		vk::PipelineDynamicStateCreateInfo* dynamicState);
};
