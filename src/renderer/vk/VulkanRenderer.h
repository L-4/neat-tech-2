#pragma once

#include "renderer/IRenderer.h"

#include "Window.h"
#include "World.h"
#include "core/Camera.h"

#include "renderer/vk/Buffer.h"
#include "renderer/vk/Context.h"
#include "renderer/vk/Image.h"
#include "renderer/vk/MemoryAllocator.h"
#include "renderer/vk/vulkan.hpp"

#include <chrono>

const u32 MAX_FRAMES_IN_FLIGHT = 2;

class VulkanRenderer : public IRenderer
{
	Context m_context;
	/** @todo: Rename to just m_allocator. */
	MemoryAllocator m_memoryAllocator;

	vk::SurfaceFormatKHR m_surfaceFormat;
	vk::PresentModeKHR m_presentMode;
	vk::Extent2D m_swapChainExtent;

	vk::UniqueSwapchainKHR m_swapChain;
	std::vector<vk::Image> m_swapChainImages;
	std::vector<vk::UniqueImageView> m_swapChainImageViews;

	vk::UniqueShaderModule m_vertexShader;
	vk::UniqueShaderModule m_fragmentShader;

	vk::UniqueDescriptorSetLayout m_descriptorSetLayout;
	vk::UniquePipelineLayout m_pipelineLayout;
	vk::UniqueRenderPass m_renderPass;

	vk::UniquePipeline m_graphicsPipeline;

	std::vector<vk::UniqueFramebuffer> m_swapchainFramebuffers;

	Buffer m_vertexBuffer;
	Buffer m_indexBuffer;
	std::vector<Buffer> m_uniformBuffers;

	Image m_image;
	vk::UniqueImageView m_imageView;
	vk::UniqueSampler m_imageSampler;

	Image m_depthImage;
	// vk::UniqueImage m_depthImage;
	// vk::UniqueDeviceMemory m_depthImageMemory;
	vk::UniqueImageView m_depthImageView;

	vk::UniqueDescriptorPool m_descriptorPool;
	std::vector<vk::DescriptorSet> m_descriptorSets;

	std::vector<vk::UniqueCommandBuffer> m_commandBuffers;

	u32 m_currentFrame = 0;

	std::vector<vk::UniqueSemaphore> m_imageAvailableSemaphores;
	std::vector<vk::UniqueSemaphore> m_renderFinishedSemaphores;
	std::vector<vk::UniqueFence> m_inFlightFences;
	/** This stores references from m_inFlightFences, so no cleanup is required. */
	std::vector<vk::Fence> m_imagesInFlight;

#ifdef TICK_TOCK
	std::chrono::high_resolution_clock m_clock;
#endif // TICK_TOCK

public:
	VulkanRenderer(const Window& window);
	~VulkanRenderer();

	void UploadCamera(const Camera& camera, u32 imageIndex) const;

	virtual void Render(const World& world,
						const Camera& camera /* , const FrameBuffer& target */) override;
};
