#include "Image.h"

#include "renderer/vk/Buffer.h"

#include "util/asserts.h"

#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>

// @todo: Deal with formats at some point.
vk::Format format = vk::Format::eR8G8B8A8Srgb;

Image::Image(Context& context, MemoryAllocator& allocator, const char* filename)
	: m_context(&context),
	  m_allocator(&allocator)
{
	// Forward declare as to not have to deal with signed int sillyness.
	u32 textureWidth = 0, textureHeight = 0, textureChannels = 0;
	stbi_uc* pixels = nullptr;

	{
		i32 _textureWidth, _textureHeight, _textureChannels;
		pixels =
			stbi_load(filename, &_textureWidth, &_textureHeight, &_textureChannels, STBI_rgb_alpha);

		textureWidth	= static_cast<u32>(_textureWidth);
		textureHeight	= static_cast<u32>(_textureHeight);
		textureChannels = static_cast<u32>(_textureChannels);
	}
	vk::DeviceSize imageSize = textureWidth * textureHeight * 4;

	Buffer imageStagingBuffer(context,
							  allocator,
							  pixels,
							  imageSize,
							  vk::BufferUsageFlagBits::eTransferSrc,
							  vk::MemoryPropertyFlagBits::eHostVisible |
								  vk::MemoryPropertyFlagBits::eHostCoherent);

	stbi_image_free(pixels);

	vk::ImageCreateInfo imageInfo {
		.imageType	   = vk::ImageType::e2D,
		.format		   = format,
		.extent		   = { .width = textureWidth, .height = textureHeight, .depth = 1 },
		.mipLevels	   = 1,
		.arrayLayers   = 1,
		.samples	   = vk::SampleCountFlagBits::e1,
		.tiling		   = vk::ImageTiling::eOptimal,
		.usage		   = vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eSampled,
		.sharingMode   = vk::SharingMode::eExclusive,
		.initialLayout = vk::ImageLayout::eUndefined,
	};

	m_image = m_context->m_device->createImageUnique(imageInfo);
	m_memory =
		m_allocator->AllocateMemory(m_context->m_device->getImageMemoryRequirements(m_image.get()),
									vk::MemoryPropertyFlagBits::eDeviceLocal);

	m_context->m_device->bindImageMemory(m_image.get(), m_memory.get(), 0);

	TransitionImageLayout(vk::ImageLayout::eUndefined, vk::ImageLayout::eTransferDstOptimal);

	vk::UniqueCommandBuffer commandBuffer = m_context->BeginSingleUseCommandBuffer();

	vk::BufferImageCopy region {
		.bufferOffset = 0,
		.bufferRowLength = 0,
		.bufferImageHeight = 0,
		.imageSubresource = {
			.aspectMask = vk::ImageAspectFlagBits::eColor,
			.mipLevel = 0,
			.baseArrayLayer = 0,
			.layerCount = 1,
		},
		.imageOffset = { .x = 0, .y = 0, .z = 0 },
		.imageExtent = { .width = textureWidth, .height = textureHeight, .depth = 1, }
	};

	commandBuffer->copyBufferToImage(imageStagingBuffer.GetBuffer(),
									 m_image.get(),
									 vk::ImageLayout::eTransferDstOptimal,
									 { region });

	m_context->EndSingleUseCommandBuffer(commandBuffer);

	TransitionImageLayout(vk::ImageLayout::eTransferDstOptimal,
						  vk::ImageLayout::eShaderReadOnlyOptimal);
}

void Image::TransitionImageLayout(vk::ImageLayout oldLayout, vk::ImageLayout newLayout)
{
	vk::UniqueCommandBuffer commandBuffer = m_context->BeginSingleUseCommandBuffer();

	vk::ImageMemoryBarrier barrier {
		.oldLayout			 = oldLayout,
		.newLayout			 = newLayout,
		.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
		.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
		.image				 = m_image.get(),
		.subresourceRange	 = { .aspectMask	 = vk::ImageAspectFlagBits::eColor,
								 .baseMipLevel	 = 0,
								 .levelCount	 = 1,
								 .baseArrayLayer = 0,
								 .layerCount	 = 1 },
	};

	vk::PipelineStageFlags srcStage, dstStage;

	if (oldLayout == vk::ImageLayout::eUndefined &&
		newLayout == vk::ImageLayout::eTransferDstOptimal)
	{
		barrier.srcAccessMask = static_cast<vk::AccessFlags>(0);
		barrier.dstAccessMask = vk::AccessFlagBits::eTransferWrite;

		srcStage = vk::PipelineStageFlagBits::eTopOfPipe;
		dstStage = vk::PipelineStageFlagBits::eTransfer;
	}
	else if (oldLayout == vk::ImageLayout::eTransferDstOptimal &&
			 newLayout == vk::ImageLayout::eShaderReadOnlyOptimal)
	{
		barrier.srcAccessMask = vk::AccessFlagBits::eTransferWrite;
		barrier.dstAccessMask = vk::AccessFlagBits::eShaderRead;

		srcStage = vk::PipelineStageFlagBits::eTransfer;
		dstStage = vk::PipelineStageFlagBits::eFragmentShader;
	}
	else
	{
		ERROR("Invalid layout transfer");
	}

	commandBuffer->pipelineBarrier(srcStage,
								   dstStage,
								   static_cast<vk::DependencyFlags>(0),
								   nullptr,
								   nullptr,
								   { barrier });

	m_context->EndSingleUseCommandBuffer(commandBuffer);
}

vk::UniqueImageView Image::CreateView()
{
	vk::ImageViewCreateInfo imageViewInfo {
		.image			  = m_image.get(),
		.viewType		  = vk::ImageViewType::e2D,
		.format			  = format,
		.subresourceRange = { .aspectMask	  = vk::ImageAspectFlagBits::eColor,
							  .baseMipLevel	  = 0,
							  .levelCount	  = 1,
							  .baseArrayLayer = 0,
							  .layerCount	  = 1 },
	};

	// @todo: This is a failure point for creating a unique object which may outlive its parent
	// object.
	return m_context->m_device->createImageViewUnique(imageViewInfo);
}

Image::Image(Context& context,
			 MemoryAllocator& allocator,
			 vk::ImageUsageFlags usage,
			 vk::MemoryPropertyFlags allocationProperties,
			 u32 width,
			 u32 height)
	: m_context(&context),
	  m_allocator(&allocator)
{
	vk::ImageCreateInfo imageInfo {
		.imageType	   = vk::ImageType::e2D,
		.format		   = format,
		.extent		   = { .width = width, .height = height, .depth = 1 },
		.mipLevels	   = 1,
		.arrayLayers   = 1,
		.samples	   = vk::SampleCountFlagBits::e1,
		.tiling		   = vk::ImageTiling::eOptimal,
		.usage		   = usage,
		.sharingMode   = vk::SharingMode::eExclusive,
		.initialLayout = vk::ImageLayout::eUndefined,
	};

	m_image = m_context->m_device->createImageUnique(imageInfo);
	m_memory =
		m_allocator->AllocateMemory(m_context->m_device->getImageMemoryRequirements(m_image.get()),
									allocationProperties);
	m_context->m_device->bindImageMemory(m_image.get(), m_memory.get(), 0);
}
