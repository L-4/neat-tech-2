#include "Buffer.h"

#include "util/asserts.h"

void Buffer::CreateBuffer(vk::DeviceSize size,
						  vk::BufferUsageFlags usage,
						  vk::MemoryPropertyFlags properties,
						  vk::UniqueBuffer& outBuffer,
						  vk::UniqueDeviceMemory& outMemory)
{
	vk::BufferCreateInfo bufferInfo { .size		   = size,
									  .usage	   = usage,
									  .sharingMode = vk::SharingMode::eExclusive };

	outBuffer = m_context->m_device->createBufferUnique(bufferInfo);
	outMemory = m_memoryAllocator->AllocateMemory(
		m_context->m_device->getBufferMemoryRequirements(outBuffer.get()),
		properties);

	m_context->m_device->bindBufferMemory(outBuffer.get(), outMemory.get(), 0);
}

void Buffer::CopyBuffer(const vk::UniqueBuffer& srcBuffer,
						const vk::UniqueBuffer& dstBuffer,
						vk::DeviceSize size)
{
	vk::CommandBufferAllocateInfo allocationInfo { .commandPool = m_context->m_commandPool.get(),
												   .level		= vk::CommandBufferLevel::ePrimary,
												   .commandBufferCount = 1 };

	// @todo: This is garbage.
	std::vector<vk::UniqueCommandBuffer> commandBuffers =
		m_context->m_device->allocateCommandBuffersUnique(allocationInfo);
	vk::CommandBuffer commandBuffer = commandBuffers.front().get();

	commandBuffer.begin({ .flags = vk::CommandBufferUsageFlagBits::eOneTimeSubmit });
	commandBuffer.copyBuffer(srcBuffer.get(), dstBuffer.get(), vk::BufferCopy { .size = size });
	commandBuffer.end();

	m_context->m_graphicsQueue.submit(
		vk::SubmitInfo { .commandBufferCount = 1, .pCommandBuffers = &commandBuffer });
	m_context->m_graphicsQueue.waitIdle();
}

void Buffer::MapBuffer(vk::DeviceMemory target, const void* data, std::size_t dataSize) const
{
	void* writePtr = m_context->m_device->mapMemory(target, 0, dataSize);
	std::memcpy(writePtr, data, dataSize);
	m_context->m_device->unmapMemory(target);
}

Buffer::Buffer(Context& context,
			   MemoryAllocator& memoryAllocator,
			   const void* data,
			   std::size_t size,
			   vk::BufferUsageFlags usage,
			   vk::MemoryPropertyFlags properties)
	: m_context(&context),
	  m_memoryAllocator(&memoryAllocator)
{
#ifdef DEBUG
	// Let's catch weird flags passed in here, since I don't entirely know their
	// purpose and validity for now.

	// Allowed configurations:
	// Index/Vertex buffer & DeviceLocal/HostVisible
	if (usage & (vk::BufferUsageFlagBits::eVertexBuffer | vk::BufferUsageFlagBits::eIndexBuffer))
	{
		ASSERT(properties & (vk::MemoryPropertyFlagBits::eDeviceLocal |
							 vk::MemoryPropertyFlagBits::eHostVisible));
	}
	// Uniform buffer & HostVisible
	else if (usage & vk::BufferUsageFlagBits::eUniformBuffer)
	{
		ASSERT(properties & (vk::MemoryPropertyFlagBits::eHostVisible));
	}
	// Transfer source buffer & HostVisible
	else if (usage & (vk::BufferUsageFlagBits::eTransferSrc))
	{
		ASSERT(properties & (vk::MemoryPropertyFlagBits::eHostVisible));
	}
	else
	{
		ERROR("Invalid usage");
	}
#endif // DEBUG

	m_isHostVisible = static_cast<bool>(properties & vk::MemoryPropertyFlagBits::eHostVisible);

	// If we're not host visible, then we'll need a staging buffer, in which
	// case this buffer is also a transfer target.
	if (!m_isHostVisible) usage |= vk::BufferUsageFlagBits::eTransferDst;
	// If we are host visible, then we need to be host coherent as well for the
	// changes to be flushed automatically.
	// @todo: Use vkFlushMappedMemoryRanges and vkInvalidateMappedMemoryRanges?
	// https://renderdoc.org/vkspec_chunked/chap12.html#VkMemoryPropertyFlagBits
	if (m_isHostVisible) properties |= vk::MemoryPropertyFlagBits::eHostCoherent;

	// Create and allocate memory for this buffer.
	CreateBuffer(size, usage, properties, m_buffer, m_memory);

	if (m_isHostVisible)
	{
		// Copy in data
		if (data) MapBuffer(m_memory.get(), data, size);
	}
	else
	{
		vk::UniqueDeviceMemory stagingBufferMemory;
		vk::UniqueBuffer stagingBuffer;

		CreateBuffer(size,
					 vk::BufferUsageFlagBits::eTransferSrc,
					 vk::MemoryPropertyFlagBits::eHostVisible |
						 vk::MemoryPropertyFlagBits::eHostCoherent,
					 stagingBuffer,
					 stagingBufferMemory);

		MapBuffer(stagingBufferMemory.get(), data, size);
		CopyBuffer(stagingBuffer, m_buffer, size);
	}
}

void Buffer::UploadData(void* data, std::size_t dataSize) const
{
	ASSERT(m_isHostVisible);
	MapBuffer(m_memory.get(), data, dataSize);
}
