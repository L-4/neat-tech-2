#pragma once

#include "renderer/vk/Context.h"
#include "renderer/vk/MemoryAllocator.h"
#include "renderer/vk/vulkan.hpp"

#include <vector>

/**
 * Wraps all buffer types.
 * @todo: Remove phyiscal device and transfer queue!
 * @todo: Use memory allocator
 */
class Buffer
{
	Context* m_context;
	MemoryAllocator* m_memoryAllocator;

	vk::UniqueDeviceMemory m_memory;
	vk::UniqueBuffer m_buffer;

	bool m_isHostVisible;

	/** Create a buffer and allocate memory for it. */
	void CreateBuffer(vk::DeviceSize size,
					  vk::BufferUsageFlags usage,
					  vk::MemoryPropertyFlags properties,
					  vk::UniqueBuffer& outBuffer,
					  vk::UniqueDeviceMemory& outMemory);

	void CopyBuffer(const vk::UniqueBuffer& srcBuffer,
					const vk::UniqueBuffer& dstBuffer,
					vk::DeviceSize size);

	void MapBuffer(vk::DeviceMemory target, const void* data, std::size_t dataSize) const;

public:
	template <typename T>
	Buffer(Context& context,
		   MemoryAllocator& memoryAllocator,
		   const std::vector<T>& from,
		   vk::BufferUsageFlags usage,
		   vk::MemoryPropertyFlags properties)
		: Buffer(context, memoryAllocator, from.data(), sizeof(T) * from.size(), usage, properties)
	{}

	Buffer(Context& context,
		   MemoryAllocator& memoryAllocator,
		   const void* data,
		   std::size_t size,
		   vk::BufferUsageFlags usage,
		   vk::MemoryPropertyFlags properties);

	/** @todo: Is it okay to have this in an uninitialized state? */
	Buffer() = default;

	operator bool() const { return m_buffer && m_memory; }
	// operator vk::Buffer() const { return m_buffer.get(); }
	vk::Buffer GetBuffer() const { return m_buffer.get(); }

	void UploadData(void* data, std::size_t dataSize) const;
};
