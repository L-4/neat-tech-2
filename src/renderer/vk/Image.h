#pragma once

#include "renderer/vk/Context.h"
#include "renderer/vk/MemoryAllocator.h"
#include "renderer/vk/vulkan.hpp"

class Image
{
public:
	vk::UniqueImage m_image;
	vk::UniqueDeviceMemory m_memory;

	Context* m_context;
	MemoryAllocator* m_allocator;

	Image(Context& context, MemoryAllocator& allocator, const char* filename);

	Image(Context& context,
		  MemoryAllocator& allocator,
		  vk::ImageUsageFlags usage,
		  vk::MemoryPropertyFlags allocationProperties,
		  u32 width,
		  u32 height);

	void TransitionImageLayout(vk::ImageLayout oldLayout, vk::ImageLayout newLayout);

	vk::UniqueImageView CreateView();

	/** @todo: Is this oakey?  */
	Image() = default;
	operator bool() const { return m_image && m_memory; }
};
