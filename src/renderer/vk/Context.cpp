#include "Context.h"

#include "util/asserts.h"

#include <SDL2/SDL_vulkan.h>
#include <set>

#ifdef DEBUG
#pragma region Debug callback

static VKAPI_ATTR VkBool32 VKAPI_CALL
debugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT /* messageSeverity */,
			  VkDebugUtilsMessageTypeFlagsEXT /* messageType */,
			  const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
			  void* /* pUserData */)
{
	printf("\nValidation layer: %s\n", pCallbackData->pMessage);

	return VK_FALSE;
}

#pragma endregion
#endif // DEBUG

Context::Context(const Window& window)
{
#pragma region vk::Instance + Debugger
	vk::ApplicationInfo appInfo { .pApplicationName	  = "Neat Tech VK",
								  .applicationVersion = VK_MAKE_VERSION(1, 0, 0),
								  .pEngineName		  = "Neat Tech VK",
								  .engineVersion	  = VK_MAKE_VERSION(1, 0, 0),
								  .apiVersion		  = VK_API_VERSION_1_2 };

	std::vector<const char*> extensions;
	window.GetRequiredExtensions(extensions);

	extensions.emplace_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);

	const std::vector<const char*> validationLayers = {
#ifdef DEBUG
		"VK_LAYER_KHRONOS_validation"
#endif // DEBUG
	};

	vk::InstanceCreateInfo instanceCreateInfo {
		.pApplicationInfo		 = &appInfo,
		.enabledLayerCount		 = static_cast<u32>(validationLayers.size()),
		.ppEnabledLayerNames	 = validationLayers.data(),
		.enabledExtensionCount	 = static_cast<u32>(extensions.size()),
		.ppEnabledExtensionNames = extensions.data(),
	};

	m_instance = createInstanceUnique(instanceCreateInfo);
	ASSERT(m_instance);

#ifdef DEBUG
	// clang-format off
	vk::DebugUtilsMessengerCreateInfoEXT messengerCreateInfo {
		.messageSeverity =
			/* vk::DebugUtilsMessageSeverityFlagBitsEXT::eVerbose | */
			/* vk::DebugUtilsMessageSeverityFlagBitsEXT::eInfo | */
			vk::DebugUtilsMessageSeverityFlagBitsEXT::eWarning |
			vk::DebugUtilsMessageSeverityFlagBitsEXT::eError,
		.messageType =
			vk::DebugUtilsMessageTypeFlagBitsEXT::eGeneral |
			vk::DebugUtilsMessageTypeFlagBitsEXT::ePerformance |
			vk::DebugUtilsMessageTypeFlagBitsEXT::eValidation,
		.pfnUserCallback = debugCallback,
	};
	// clang-format on

	m_debugUtilsMessenger = m_instance->createDebugUtilsMessengerEXTUnique(messengerCreateInfo);
	ASSERT(m_debugUtilsMessenger);
#endif // DEBUG

#pragma endregion

#pragma region Create surface

	// @todo: Some handle juggling is going on here, can probably be expressed
	// in a better way.
	VkSurfaceKHR tempSurface;

	SDL_Vulkan_CreateSurface(window.m_sdlWindow,
							 static_cast<VkInstance>(m_instance.get()),
							 &tempSurface);

	vk::ObjectDestroy<vk::Instance, VULKAN_HPP_DEFAULT_DISPATCHER_TYPE> _deleter(m_instance.get());
	m_surface = vk::UniqueSurfaceKHR(vk::SurfaceKHR(tempSurface), _deleter);

	ASSERT(m_surface);

#pragma endregion

#pragma region Find GPU + Queue families
	// Find a device which will satisfy all of our requirements later when we create
	// swachain etc.

	u32 graphicsFamily, presentFamily;
	const std::array requiredDeviceExtensions { VK_KHR_SWAPCHAIN_EXTENSION_NAME };

	for (const vk::PhysicalDevice& physicalDevice : m_instance->enumeratePhysicalDevices())
	{
		// Make sure device has present support.
		u32 numFoundRequiredExtensions = 0;

		for (const vk::ExtensionProperties& extension :
			 physicalDevice.enumerateDeviceExtensionProperties())
		{
			for (const char* requiredExtension : requiredDeviceExtensions)
			{
				if (strcmp(requiredExtension, extension.extensionName.data()) == 0)
				{
					++numFoundRequiredExtensions;
					break;
				}
			}

			if (numFoundRequiredExtensions == requiredDeviceExtensions.size()) break;
		}

		if (numFoundRequiredExtensions != requiredDeviceExtensions.size()) continue;

		// Make sure that device has at least one present mode.
		u32 surfacePresentModeCount = 0;
		ASSERT(physicalDevice.getSurfacePresentModesKHR(m_surface.get(),
														&surfacePresentModeCount,
														nullptr) == vk::Result::eSuccess);

		if (surfacePresentModeCount == 0) continue;

		// Make sure that device has at least one surface format.
		u32 surfaceFormatsCount = 0;
		ASSERT(physicalDevice.getSurfaceFormatsKHR(m_surface.get(),
												   &surfaceFormatsCount,
												   nullptr) == vk::Result::eSuccess);

		if (surfaceFormatsCount == 0) continue;

		// Make sure that device has graphics and present queue families.
		std::optional<u32> graphicsFamilyOption;
		std::optional<u32> presentFamilyOption;

		u32 qfIndex = 0;
		for (const vk::QueueFamilyProperties& qfProperties :
			 physicalDevice.getQueueFamilyProperties())
		{
			if (qfProperties.queueFlags & vk::QueueFlagBits::eGraphics)
				graphicsFamilyOption = qfIndex;
			if (physicalDevice.getSurfaceSupportKHR(qfIndex, m_surface.get()))
				presentFamilyOption = qfIndex;

			++qfIndex;
		}

		if (graphicsFamilyOption.has_value() && presentFamilyOption.has_value())
		{
			m_physicalDevice = physicalDevice;
			graphicsFamily	 = graphicsFamilyOption.value();
			presentFamily	 = presentFamilyOption.value();

			break;
		}
	}

	ASSERT_MSG(m_physicalDevice, "Could not find valid GPU!");

#pragma endregion

#pragma region Make logical device

	std::set<u32> uniqueQueueFamilies = { graphicsFamily, presentFamily };
	std::vector<vk::DeviceQueueCreateInfo> queueCreateInfos;

	float queuePriority = 1.0f;
	for (const u32 queueFamilyIndex : uniqueQueueFamilies)
	{
		queueCreateInfos.emplace_back(
			vk::DeviceQueueCreateInfo { .queueFamilyIndex = queueFamilyIndex,
										.queueCount		  = 1,
										.pQueuePriorities = &queuePriority });
	}

	vk::PhysicalDeviceFeatures deviceFeatures {};

	// @todo: Array proxy?
	vk::DeviceCreateInfo deviceCreateInfo {
		.queueCreateInfoCount	 = static_cast<u32>(queueCreateInfos.size()),
		.pQueueCreateInfos		 = queueCreateInfos.data(),
		.enabledExtensionCount	 = static_cast<u32>(requiredDeviceExtensions.size()),
		.ppEnabledExtensionNames = requiredDeviceExtensions.data(),
		.pEnabledFeatures		 = &deviceFeatures,
	};

	m_device = m_physicalDevice.createDeviceUnique(deviceCreateInfo);

	/** @todo: This should also be removed. */
	m_graphicsFamilyIndex = graphicsFamily;
	m_presentFamilyIndex  = presentFamily;

	m_commandPool = m_device->createCommandPoolUnique({ .queueFamilyIndex = graphicsFamily });

	// Also get queue handles, while we're at it.
	m_graphicsQueue = m_device->getQueue(graphicsFamily, 0);
	m_presentQueue	= m_device->getQueue(presentFamily, 0);
#pragma endregion
}

vk::UniqueCommandBuffer Context::BeginSingleUseCommandBuffer()
{
	vk::CommandBufferAllocateInfo commandBufferInfo { .commandPool = m_commandPool.get(),
													  .level = vk::CommandBufferLevel::ePrimary,
													  .commandBufferCount = 1 };

	std::vector<vk::UniqueCommandBuffer> commandBuffers =
		m_device->allocateCommandBuffersUnique(commandBufferInfo);
	vk::UniqueCommandBuffer commandBuffer = std::move(commandBuffers.front());

	commandBuffer->begin({ .flags = vk::CommandBufferUsageFlagBits::eOneTimeSubmit });

	return commandBuffer;
}

void Context::EndSingleUseCommandBuffer(vk::UniqueCommandBuffer& commandBuffer)
{
	commandBuffer->end();

	vk::CommandBuffer commandBufferRef = commandBuffer.get();
	vk::SubmitInfo submitInfo { .commandBufferCount = 1, .pCommandBuffers = &commandBufferRef };

	m_graphicsQueue.submit(submitInfo);
	m_graphicsQueue.waitIdle();
}
