#pragma once

#include "renderer/vk/Context.h"
#include "renderer/vk/vulkan.hpp"

class MemoryAllocator
{
	Context* m_context;

public:
	MemoryAllocator(Context& context) : m_context(&context) {}

	vk::UniqueDeviceMemory AllocateMemory(vk::MemoryRequirements requirements,
										  vk::MemoryPropertyFlags properties);
};
