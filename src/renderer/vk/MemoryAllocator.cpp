#include "MemoryAllocator.h"

#include "util/asserts.h"

vk::UniqueDeviceMemory MemoryAllocator::AllocateMemory(vk::MemoryRequirements requirements,
													   vk::MemoryPropertyFlags properties)
{
	vk::PhysicalDeviceMemoryProperties memProperties =
		m_context->m_physicalDevice.getMemoryProperties();
	u32 memoryType = UINT32_MAX;

	for (u32 i = 0; i < requirements.size; ++i)
	{
		if ((requirements.memoryTypeBits & (1 << i)) &&
			(memProperties.memoryTypes[i].propertyFlags & properties) == properties)
		{
			memoryType = i;
			break;
		}
	}

	ASSERT(memoryType != UINT32_MAX);

	vk::MemoryAllocateInfo allocationInfo { .allocationSize	 = requirements.size,
											.memoryTypeIndex = memoryType };

	return m_context->m_device->allocateMemoryUnique(allocationInfo);
}
