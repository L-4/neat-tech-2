#pragma once

#include "renderer/vk/vulkan.hpp"

#include "Window.h"

/**
 * Deals with context objects which are generally "static", in that we create
 * them at startup time and don't touch them past that.
 *
 * @todo: In the future we might want to have multiple instances of the members
 * of this class, in which case this abstraction stops making sense. If so,
 * this should probably just be deleted.
 *
 * It's even more likely that we'll want more vk::Queue objects, which means
 * that we'll have to start storing the source queue for objects.
 */
class Context
{
public:
	/** Analogous to an OpenGL instance. */
	vk::UniqueInstance m_instance;
#ifdef DEBUG
	/** Debug messenger. I don't know why this is an object. */
	vk::UniqueDebugUtilsMessengerEXT m_debugUtilsMessenger;
#endif // DEBUG

	/** Handle to physical GPU. */
	vk::PhysicalDevice m_physicalDevice;
	/** Logical device. */
	vk::UniqueDevice m_device;
	vk::UniqueSurfaceKHR m_surface;

	vk::Queue m_graphicsQueue;
	vk::Queue m_presentQueue;

	vk::UniqueCommandPool m_commandPool;

	/**
	 * @todo: This should not be exposed here. Only the swapchain requires it,
	 * maybe that should be in here?
	 */
	u32 m_graphicsFamilyIndex;
	u32 m_presentFamilyIndex;

	Context(const Window& window);

	vk::UniqueCommandBuffer BeginSingleUseCommandBuffer();
	void EndSingleUseCommandBuffer(vk::UniqueCommandBuffer& commandBuffer);
};
