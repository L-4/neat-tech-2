#pragma once

// Sets up the vulkan c++ wrapper.

#define VULKAN_HPP_NO_STRUCT_CONSTRUCTORS

#include <vulkan/vulkan.hpp>
