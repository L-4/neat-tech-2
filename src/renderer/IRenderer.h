#pragma once

#include "World.h"
#include "core/Camera.h"

class IRenderer
{
public:
	virtual void Render(const World& world,
						const Camera& camera /* , const FrameBuffer& target */) = 0;
};
