#include "Window.h"

#include "util/asserts.h"

#include <SDL2/SDL_vulkan.h>

Window::Window(u32 width, u32 height) : m_width(width), m_height(height)
{
	// Initialize SDL. Returns != 0 if error.
	ASSERT_MSG(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) == 0,
			   "SDL could not initialize! SDL_Error: %s",
			   SDL_GetError())

	m_sdlWindow = SDL_CreateWindow("Neat Tech VK",
								   SDL_WINDOWPOS_UNDEFINED,
								   SDL_WINDOWPOS_UNDEFINED,
								   width,
								   height,
								   SDL_WINDOW_SHOWN | SDL_WINDOW_VULKAN);

	ASSERT_MSG(m_sdlWindow, "Window could not be created! SDL_Error: %s", SDL_GetError());
}

Window::~Window()
{
	if (!m_sdlWindow) return;
	SDL_DestroyWindow(m_sdlWindow);
	m_sdlWindow = nullptr;
	SDL_Quit();
}

void Window::SetMouseGrab(bool newGrab)
{
	SDL_SetWindowGrab(m_sdlWindow, static_cast<SDL_bool>(newGrab));
	SDL_SetRelativeMouseMode(static_cast<SDL_bool>(newGrab));
}

bool Window::HasMouseLock() const
{
	return SDL_GetRelativeMouseMode();
}

// void Window::GetRequiredExtensions(Array<const char*>& outExtensions) const
// {
// 	u32 extensionCount;
// 	ASSERT(SDL_Vulkan_GetInstanceExtensions(m_sdlWindow, &extensionCount, nullptr));

// 	ASSERT(SDL_Vulkan_GetInstanceExtensions(m_sdlWindow,
// 											&extensionCount,
// 											outExtensions.GetWritablePtr(extensionCount)));
// }

void Window::GetFramebufferSize(i32* outWidth, i32* outHeight) const
{
	SDL_Vulkan_GetDrawableSize(m_sdlWindow, outWidth, outHeight);
}

// @todo: Optimize this method to write directly to the vector, or don't.
void Window::GetRequiredExtensions(std::vector<const char*>& outExtensions) const
{
	u32 extensionCount;
	ASSERT(SDL_Vulkan_GetInstanceExtensions(m_sdlWindow, &extensionCount, nullptr));

	std::vector<const char*> requiredExtensions(extensionCount);
	ASSERT(
		SDL_Vulkan_GetInstanceExtensions(m_sdlWindow, &extensionCount, requiredExtensions.data()));

	for (const char* extension : requiredExtensions) outExtensions.push_back(extension);
}
