#pragma once

#include <SDL2/SDL_rwops.h>

#include <vector>

class File
{
	SDL_RWops* m_fileHandle = nullptr;

public:
	// enum class

	File(const char* filename, const char* mode);
	~File();

	const std::vector<u8> ReadWholeFile() const;

	operator bool() const { return m_fileHandle != nullptr; }
};
