#pragma once

#include "Transform.h"

#include "containers/Array.h"
#include "util/ref.h"

// Until a better design is made, there will be no children, no different
// types of nodes, etc.

// @todo: Node is a bad name.
class Node
{
public:
	Transform m_transform;

	// Transform m_worldTransform;
	// Array<Ref<Node>> m_children;
	// Ref<Node> m_parent;

	// void AddChild(Ref<Node> newChild);
	// void RemoveChild(Ref<Node> child);
	// void RemoveAllChildren();
};

// Object - maybe
// Node - Hierarchy
// Spatial
