#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

#include <stdio.h>

class Transform
{
private:
	glm::vec3 m_translation = glm::vec3();

	glm::quat m_rotation = glm::quat(1.0, 0.0, 0.0, 0.0);

	/** Thanks to UE for the name idea. (Doesn't interfere with Scale()) */
	glm::vec3 m_scale = glm::vec3(1.0, 1.0, 1.0);

	enum class EDirtyFlag : u32
	{
		Matrix	  = 1 << 0,
		InvMatrix = 1 << 1,
		All		  = Matrix | InvMatrix,
	};

	/** These are mutable as to be able to be updated during a const GetMatrix(); */
	mutable glm::mat4 m_cachedMatrix	= glm::mat4();
	mutable glm::mat4 m_cachedInvMatrix = glm::mat4();

	/** Maybe bool bMatrixDirty : 1; etc would make more sense. */
	mutable u32 m_dirtyFlags = static_cast<u32>(EDirtyFlag::All);

private:
	void RecalculateMatrix() const;
	void RecalculateInvMatrix() const;

	bool IsDirty(EDirtyFlag flag) const { return m_dirtyFlags & static_cast<u32>(flag); }
	void MakeDirty(EDirtyFlag flag) const { m_dirtyFlags |= static_cast<u32>(flag); }
	void RemoveDirty(EDirtyFlag flag) const { m_dirtyFlags &= ~static_cast<u32>(flag); }

public:
	// Getters/setters
	const glm::vec3& GetTranslation() const { return m_translation; }
	void SetTranslation(const glm::vec3& newTranslation);

	const glm::quat& GetRotation() const { return m_rotation; }
	void SetRotation(const glm::quat& newRotation);
	void SetRotationFromEuler(const glm::vec3& euler);
	void ResetRotation();

	const glm::vec3& GetScale() const { return m_scale; }
	void SetScale(const glm::vec3& newScale);

	// There could be a setter for the matrix, but does that ever make sense?
	/** Gets the matrix, recalculating it if required. */
	const glm::mat4& GetMatrix() const;
	/** Gets the inverse matrix, recalculating it if required. */
	const glm::mat4& GetInvMatrix() const;

	// Methods which mutate.
	void Translate(const glm::vec3& by);

	void RotateX(const f32 by);
	void RotateY(const f32 by);
	void RotateZ(const f32 by);

	void Scale(const f32 by);
	void Scale(const glm::vec3& by);

	void Xform(const glm::mat4& by);
	void Xform(const Transform& by);
};
