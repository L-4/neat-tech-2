#include "Transform.h"

void Transform::RecalculateMatrix() const
{
	// *mat4.identity(dest);
	// *mat4.translate(dest, vec);
	// *let quatMat = mat4.create();
	// *quat4.toMat4(quat, quatMat);
	// *mat4.multiply(dest, quatMat);
	// *mat4.scale(dest, scale)
	m_cachedMatrix	  = glm::mat4(1.0);
	m_cachedMatrix	  = glm::translate(m_cachedMatrix, m_translation);
	glm::mat4 QuatMat = glm::mat4_cast(m_rotation);
	m_cachedMatrix *= QuatMat;
	m_cachedMatrix = glm::scale(m_cachedMatrix, m_scale);

	// @todo: Should be equivalent??????
	RemoveDirty(EDirtyFlag::Matrix);
}

void Transform::RecalculateInvMatrix() const
{
	if (IsDirty(EDirtyFlag::InvMatrix)) RecalculateMatrix();

	m_cachedInvMatrix = glm::inverse(m_cachedMatrix);
	RemoveDirty(EDirtyFlag::InvMatrix);
}

void Transform::SetTranslation(const glm::vec3& newTranslation)
{
	m_translation = newTranslation;
	MakeDirty(EDirtyFlag::All);
}

void Transform::SetRotation(const glm::quat& newRotation)
{
	m_rotation = newRotation;
	MakeDirty(EDirtyFlag::All);
}

void Transform::SetRotationFromEuler(const glm::vec3& euler)
{
	m_rotation = glm::quat(euler);
	MakeDirty(EDirtyFlag::All);
}

void Transform::ResetRotation()
{
	m_rotation = glm::quat(1.0, 0.0, 0.0, 0.0);
	MakeDirty(EDirtyFlag::All);
}

void Transform::SetScale(const glm::vec3& newScale)
{
	m_scale = newScale;
	MakeDirty(EDirtyFlag::All);
}

const glm::mat4& Transform::GetMatrix() const
{
	if (IsDirty(EDirtyFlag::InvMatrix)) RecalculateMatrix();

	return m_cachedMatrix;
}

const glm::mat4& Transform::GetInvMatrix() const
{
	if (IsDirty(EDirtyFlag::InvMatrix)) RecalculateInvMatrix();

	return m_cachedInvMatrix;
}

void Transform::Translate(const glm::vec3& by)
{
	m_translation += by;
	MakeDirty(EDirtyFlag::All);
}

/** @todo: Do these three even do what they say that they do? */
void Transform::RotateX(const f32 by)
{
	/** @todo: Surely there are constants?... Applies to next three methods. */
	m_rotation = glm::rotate(m_rotation, by, glm::vec3(1.0, 0.0, 0.0));
	MakeDirty(EDirtyFlag::All);
}

void Transform::RotateY(const f32 by)
{
	m_rotation = glm::rotate(m_rotation, by, glm::vec3(0.0, 1.0, 0.0));
	MakeDirty(EDirtyFlag::All);
}

void Transform::RotateZ(const f32 by)
{
	m_rotation = glm::rotate(m_rotation, by, glm::vec3(0.0, 0.0, 1.0));
	MakeDirty(EDirtyFlag::All);
}

void Transform::Scale(const f32 by)
{
	m_scale *= by;
	MakeDirty(EDirtyFlag::All);
}

void Transform::Scale(const glm::vec3& by)
{
	m_scale *= by;
	MakeDirty(EDirtyFlag::All);
}
