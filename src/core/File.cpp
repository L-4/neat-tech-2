#include "File.h"

#include "util/asserts.h"

File::File(const char* filename, const char* mode)
{
	m_fileHandle = SDL_RWFromFile(filename, mode);

	ASSERT(m_fileHandle);
}

File::~File()
{
	if (m_fileHandle)
	{
		SDL_RWclose(m_fileHandle);
		m_fileHandle = nullptr;
	}
}

const std::vector<u8> File::ReadWholeFile() const
{
	SDL_RWseek(m_fileHandle, 0, RW_SEEK_SET);
	const i64 fileSize = SDL_RWsize(m_fileHandle);
	std::vector<u8> data(fileSize);
	SDL_RWread(m_fileHandle, data.data(), fileSize, 1);

	return data;
}
