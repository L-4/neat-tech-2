#include "Camera.h"

#include "Engine.h"
#include "InputManager.h"
#include "globals.h"

// @todo: Remove hard coded default resolution.
Camera::Camera(f32 fov, u32 width, u32 height) : m_width(width), m_height(height), m_fov(fov)
{
	f32 aspectRatio = static_cast<f32>(m_width) / static_cast<f32>(m_height);
	m_projection	= glm::perspective(glm::radians(m_fov), aspectRatio, 0.1f, 1000.0f);

	g_inputManager->MouseMotionEventDelegate.Bind(this, &Camera::OnMouseMotion);
	g_inputManager->KeyboardEventDelegate.Bind(this, &Camera::OnKeyboard);
	g_engine->OnUpdate.Bind(this, &Camera::OnUpdate);
}

/** This... shouldn't be right. But it works, so? */
glm::vec3 Camera::GetForwardVector() const
{
	f32 cosPitch = sin(m_pitch);

	return glm::vec3(-sin(m_yaw) * cosPitch, cos(m_yaw) * cosPitch, -cos(m_pitch));
}

glm::vec3 Camera::GetRightVector() const
{
	return glm::vec3(cos(m_yaw), sin(m_yaw), 0.0);
}

glm::vec3 Camera::GetUpVector() const
{
	return glm::vec3(0.0, 0.0, 1.0);
}

void Camera::Resize(u32 newWidth, u32 newHeight) const
{
	if (newWidth == m_width && newHeight == m_height) return;

	f32 aspectRatio = static_cast<f32>(newWidth) / static_cast<f32>(newHeight);
	m_projection	= glm::perspective(m_fov, aspectRatio, 0.1f, 1000.0f);
	m_width			= newWidth;
	m_height		= newHeight;
}

void Camera::OnMouseMotion(const SDL_MouseMotionEvent& mouseMotion)
{
	if (!g_window->HasMouseLock()) return;

	const f32 relativeX = static_cast<f32>(mouseMotion.xrel);
	const f32 relativeY = static_cast<f32>(mouseMotion.yrel);

	m_pitch = std::max(std::min(glm::pi<f32>(), m_pitch - relativeY * m_sensitivity), 0.0f);
	m_yaw -= relativeX * m_sensitivity;
	m_transform.SetRotationFromEuler(glm::vec3(m_pitch, 0.0f, m_yaw));
}

void Camera::OnUpdate(f32 delta)
{
	glm::vec3 movementVector(0.0);

	if (m_forwardMovement) movementVector += GetForwardVector() * m_forwardMovement;
	if (m_rightMovement) movementVector += GetRightVector() * m_rightMovement;
	if (m_upMovement) movementVector += GetUpVector() * m_upMovement;

	if (m_forwardMovement || m_rightMovement || m_upMovement)
	{ m_transform.Translate(glm::normalize(movementVector) * (delta * m_velocity)); }
}

void Camera::OnKeyboard(const SDL_KeyboardEvent& keyboardEvent)
{

	switch (keyboardEvent.keysym.sym)
	{
		case SDLK_w:
			m_forwardMovement = keyboardEvent.state == SDL_PRESSED ? 1.0f : 0.0f;
			break;
		case SDLK_s:
			m_forwardMovement = keyboardEvent.state == SDL_PRESSED ? -1.0f : 0.0f;
			break;
		case SDLK_d:
			m_rightMovement = keyboardEvent.state == SDL_PRESSED ? 1.0f : 0.0f;
			break;
		case SDLK_a:
			m_rightMovement = keyboardEvent.state == SDL_PRESSED ? -1.0f : 0.0f;
			break;
		case SDLK_SPACE:
			m_upMovement = keyboardEvent.state == SDL_PRESSED ? 1.0f : 0.0f;
			break;
		case SDLK_LSHIFT:
			m_upMovement = keyboardEvent.state == SDL_PRESSED ? -1.0f : 0.0f;
			break;
	}
}
