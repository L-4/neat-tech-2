#include "SharedObject.h"

SharedObject::~SharedObject()
{
	Free();
}

bool SharedObject::Load()
{
	m_object = SDL_LoadObject(m_fileName);
	if (!m_object) { return false; }
	return LoadFunctions();
}

void SharedObject::Free()
{
	if (m_object)
	{
		SDL_UnloadObject(m_object);
		m_object = nullptr;
	}
}

bool SharedObject::Reload()
{
	Free();
	return Load();
}
