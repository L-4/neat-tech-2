#pragma once

#include "core/Transform.h"
#include <glm/glm.hpp>

#include <SDL2/SDL_events.h>

class Camera
{
public:
	Camera(f32 fov, u32 width, u32 height);

private:
	/**
	 * View transform.
	 * @todo Would it better to just store the translation/rotation values here
	 * directly, since we dont't actually need a full transform?
	 */
	Transform m_transform;

	/** Projection matrix. */
	mutable glm::mat4 m_projection;
	mutable u32 m_width;
	mutable u32 m_height;

	/** Fov, used to make projection matrix. */
	f32 m_fov;
	/** Internal pitch, used to construct view matrix. */
	f32 m_pitch = 0.0f;
	/** Internal yaw, used to construct view matrix. */
	f32 m_yaw = 0.0f;
	/** Speed at which camera moves in freelook. */
	f32 m_velocity = 1.0f;
	/** Mouse sensitivity. */
	f32 m_sensitivity = 0.001f;

	/** @todo: Reimplement in a way that isn't garbage. */
	glm::vec3 GetForwardVector() const;
	f32 m_forwardMovement = 0.0f;
	glm::vec3 GetRightVector() const;
	f32 m_rightMovement = 0.0f;
	glm::vec3 GetUpVector() const;
	f32 m_upMovement = 0.0f;

public:
	const Transform& GetTransform() const { return m_transform; }
	const glm::mat4& GetProjection() const { return m_projection; }

	/** Recreate projection using new size. */
	void Resize(u32 newWidth, u32 newHeight) const;

	/** Callbacks to move camera in freelook. */
	void OnMouseMotion(const SDL_MouseMotionEvent& mouseMotion);
	void OnUpdate(f32 delta);
	void OnKeyboard(const SDL_KeyboardEvent& keyboardEvent);
};
