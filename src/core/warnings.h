#pragma once

/**
 * Compiler defines
 * source:
 * https://blog.kowalczyk.info/article/j/guide-to-predefined-macros-in-c-compilers-gcc-clang-msvc-etc..html
 *
 * Visual Studio       _MSC_VER
 * gcc                 __GNUC__
 * clang               __clang__
 * emscripten          __EMSCRIPTEN__ (for asm.js and webassembly)
 * MinGW 32            __MINGW32__
 * MinGW-w64 32bit     __MINGW32__
 * MinGW-w64 64bit     __MINGW64__
 */

#if defined(COMPILER_CLANG)

// Clang isn't smart enough to understand that #pragma gcc isn't it's concern.
#pragma clang diagnostic ignored "-Wunknown-warning-option"

#elif defined(COMPILER_GCC)

// Since nothing can be easy:
// This pragma is ignored. Instead, we have to inject "-Wno-unknown-pragmas" into the compiler args.
// https://stackoverflow.com/a/31510027
#pragma GCC diagnostic ignored "-Wunknown-pragmas"

#endif // defined(__GNUC__)
