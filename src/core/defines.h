#pragma once

// Deals with all platform/compiler specific defines, to make sure that any
// issues can be solved right here

/**
 * Compiler defines
 * source:
 * https://blog.kowalczyk.info/article/j/guide-to-predefined-macros-in-c-compilers-gcc-clang-msvc-etc..html
 *
 * Visual Studio       _MSC_VER
 * gcc                 __GNUC__
 * clang               __clang__
 * emscripten          __EMSCRIPTEN__ (for asm.js and webassembly)
 * MinGW 32            __MINGW32__
 * MinGW-w64 32bit     __MINGW32__
 * MinGW-w64 64bit     __MINGW64__
 */

// Compilers
#if defined(_MSC_VER)
#define COMPILER_MSVC
#endif // defined(_MSC_VER)

#if defined(__GNUC__)
#define COMPILER_GCC
#endif // defined(__GNUC__)

#if defined(__clang__)
#define COMPILER_CLANG
#endif // defined(__clang__)

#if defined(__EMSCRIPTEN__)
#define COMPILER_EMCC
#endif // defined(__EMSCRIPTEN__)

#if defined(__MINGW32__) || defined(__MINGW64__)
#define COMPILER_MINGW
#endif // defined(__MINGW32__) || defined(__MINGW64__)

// Platforms
// These are defined in the Makefile. Just make sure that they're reasonable here.

// clang-format off

#if defined(PLATFORM_LINUX) + \
    defined(PLATFORM_WINDOWS) != 1
#error "More than one (or no) platforms defined!"
#endif

// clang-format on
