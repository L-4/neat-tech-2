#pragma once

#include <SDL2/SDL_loadso.h>

#include "util/asserts.h"

#define SO_FUNC_NAME(NAME) __##NAME##_t
#define SO_FUNC_TYPEDEF(NAME, RET, ...) extern "C" typedef RET (*SO_FUNC_NAME(NAME))(__VA_ARGS__)
#define SO_HEADER_DEF(NAME) SO_FUNC_NAME(NAME) NAME;
#define SO_LOAD_FN(NAME, RET, ...)                                                                 \
	NAME = (SO_FUNC_NAME(NAME))SDL_LoadFunction(m_object, #NAME);                                  \
	ASSERT_MSG(NAME, "Could not load '%s' from '%s'!", #NAME, m_fileName);

class SharedObject
{
public:
	const char* m_fileName;
	void* m_object = nullptr;

	SharedObject(const char* fileName) : m_fileName(fileName) {};
	~SharedObject();

	/** Load object.*/
	bool Load();
	virtual bool LoadFunctions() = 0;
	/** Frees the object, if present. */
	void Free();
	/** Frees and reloads the object. */
	bool Reload();

	operator bool() const { return m_object != nullptr; }
};
