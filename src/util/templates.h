#pragma once

// clang-format off

/**
 * TRemoveReference<type> will remove any references from a type.
 */
template <typename T> struct TRemoveReference { typedef T Type; };
template <typename T> struct TRemoveReference<T&> { typedef T Type; };
template <typename T> struct TRemoveReference<T&&> { typedef T Type; };

/**
 * Forward will cast a reference to an rvalue reference.
 * This is UE's equivalent of std::forward.
 */
template <typename T>
T&& Forward(typename TRemoveReference<T>::Type& Obj)
{
	return (T &&) Obj;
}

template <typename T>
T&& Forward(typename TRemoveReference<T>::Type&& Obj)
{
	return (T &&) Obj;
}

// clang-format on
