#pragma once

namespace Char
{

inline bool IsDigit(char c)
{
	return c >= '0' && c <= '9';
}

inline char ToLowercase(char c)
{
	return c | 32;
}

inline char ToUppercase(char c)
{
	return c & ~(32);
}

/** Alphabetic???? Can't think of a better word, refuse to look it up. */
inline bool IsAlphabetic(char c)
{
	char lowered = ToLowercase(c);
	return lowered >= 'a' && lowered <= 'z';
}

inline bool IsAlphanumeric(char c)
{
	return IsDigit(c) || IsAlphabetic(c);
}

inline bool IsQuote(char c)
{
	return c == '"' || c == '\'';
}

}; // namespace Char
