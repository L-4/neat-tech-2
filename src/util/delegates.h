#pragma once

#include "containers/Array.h"
#include "util/asserts.h"

class Dummy;

template <typename TCallType>
struct TMethodBind
{
	Dummy* m_this;
	TCallType m_delegate;

	TMethodBind(Dummy* m_this, TCallType m_delegate) : m_this(m_this), m_delegate(m_delegate) {}
	TMethodBind() = default;
};

/**
 * Bad delegate implementation. This is not meant to be good, it is meant to work and be improved
 * upon as I figure this out.
 *
 * @todo: It's a nice name, but not accurate. This is a container for delegates, not a delegate.
 * @todo: It might be worth having a second delegate type for events which can be cancelled, such as
 * events.
 */
template <typename... TParameterTypes>
class TDelegate
{
private:
	typedef void (Dummy::*CallType)(TParameterTypes...);
	Array<TMethodBind<CallType>> Binds;

public:
	template <typename T>
	void Bind(T* m_this, void (T::*m_delegate)(TParameterTypes...))
	{
		Binds.Emplace(
			TMethodBind(reinterpret_cast<Dummy*>(m_this), reinterpret_cast<CallType>(m_delegate)));
	}

	void Call(TParameterTypes... Args)
	{
		for (const auto& Cb : Binds)
		{
			ASSERT(Cb.m_this);
			ASSERT(Cb.m_delegate);
			(Cb.m_this->*Cb.m_delegate)(Args...);
		}
	}
};
