#include "debugprint.h"

#include <stdio.h>

namespace DebugPrint
{
void Print(const glm::mat4& matrix)
{
	printf("glm::mat4(\n"
		   "%5.2f, %5.2f, %5.2f, %5.2f,\n"
		   "%5.2f, %5.2f, %5.2f, %5.2f,\n"
		   "%5.2f, %5.2f, %5.2f, %5.2f,\n"
		   "%5.2f, %5.2f, %5.2f, %5.2f);\n",
		   matrix[0][0],
		   matrix[1][0],
		   matrix[2][0],
		   matrix[3][0],
		   matrix[0][1],
		   matrix[1][1],
		   matrix[2][1],
		   matrix[3][1],
		   matrix[0][2],
		   matrix[1][2],
		   matrix[2][2],
		   matrix[3][2],
		   matrix[0][3],
		   matrix[1][3],
		   matrix[2][3],
		   matrix[3][3]);
}

void Print(const glm::vec3& vec)
{
	printf("glm::vec3(%5.2f, %5.2f, %5.2f);\n", vec.x, vec.y, vec.z);
}

} // namespace DebugPrint
