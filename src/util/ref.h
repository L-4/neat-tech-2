#pragma once

#include "util/templates.h"

#include "util/asserts.h"

// All these rules can still seem very complicated and headache inducing. I personally over-simplify
// this for myself (if I thereby shoot myself in the foot then so be it: I guess I will spend 2 days
// in the hospital rather than having a couple of dozen days of headaches)
// - https://stackoverflow.com/a/33988299

/** Generic non nullable smart pointer. Inspired by UE. */
template <class T>
class Ref
{
	/** Pointer to refcount. */
	i32* m_count;
	/** Whether this should be deleted with delete[]. @todo: This sucks. */
	bool m_isArray;

public:
	/** Pointer to stored data. */
	T* m_data;

	Ref() : Ref(new T) {}

	Ref(T* ptr, bool isArray = false) : m_count(new i32(1)), m_isArray(isArray), m_data(ptr)
	{
		ASSERT(m_data);
		ASSERT(m_count);
	}

	Ref(const Ref<T>& other) { CopyFrom(other); }
	~Ref() { Release(); }

	Ref<T>& operator=(const Ref<T>& other)
	{
		if (this != &other) { CopyFrom(other); }

		return *this;
	}

	T* operator->() { return m_data; }
	const T* operator->() const { return m_data; }
	T& operator*() { return *m_data; }

	/** The same as Ref<T>.m_data, but makes it more obvious that you're accessing a raw pointer. */
	T* Ptr() { return m_data; }

private:
	/** Increment refcount. */
	void AddRef() { ++(*m_count); }
	/** Decrement refcount and return new refcount. */
	void RemoveRef() { --(*m_count); }
	/** Decrement refcount and delete if needed. */
	void Release()
	{
		RemoveRef();
		if (*m_count == 0)
		{
			if (m_isArray) { delete[] m_data; }
			else
			{
				delete m_data;
			}
			delete m_count;
		}
	}

	void CopyFrom(const Ref<T>& other)
	{
		m_data	  = other.m_data;
		m_count	  = other.m_count;
		m_isArray = other.m_isArray;
		AddRef();
	}
};

/** @todo: move */
template <typename T, typename... TArgs>
Ref<T> MakeRef(TArgs... args)
{
	return Ref<T>(new T(args...));
}

// /** @todo: move */
// template <typename T, typename... TArgs>
// Ref<T> MakeRef(TArgs&&... args)
// {
// 	return Ref<T>(new T(Forward<TArgs>(args)...));
// }
