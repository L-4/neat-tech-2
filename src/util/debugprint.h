#pragma once

#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>

namespace DebugPrint
{
void Print(const glm::mat4& matrix);
void Print(const glm::vec3& vec);
} // namespace DebugPrint
