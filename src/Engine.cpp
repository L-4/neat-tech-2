#include "Engine.h"
#include "SDL2/SDL_timer.h"

#include <SDL2/SDL.h>

Engine* g_engine;

Engine::Engine()
{

	m_isRunning = true;

#ifdef DEBUG
	PrintVersions();
#endif // DEBUG
}

void Engine::Initialize()
{
	ASSERT(!g_engine);

	g_engine = new Engine();
}

void Engine::PrintVersions() const
{
	{ // SDL
		SDL_version version;
		SDL_GetVersion(&version);
		printf("SDL version: %d.%d.%d\n", version.major, version.minor, version.patch);
	}

	// @cleanup: This is functional but ugly.
	{ // Engine build profiles
		printf("NT Built with (");

#ifdef DEBUG
		printf("debug ");
#endif // DEBUG

		printf(")\n");
	}
}

void Engine::Quit(i32 WithExitStatus)
{
	m_isRunning = false;

	m_exitStatus = WithExitStatus;
}

i32 Engine::BeginMainLoop(std::function<void(void)> loopFunction)
{
	u32 lastTime = SDL_GetTicks();

	while (m_isRunning)
	{
		u32 now	  = SDL_GetTicks();
		f32 delta = static_cast<f32>(now - lastTime) * 0.001;
		lastTime  = now;

		OnUpdate.Call(delta);

		loopFunction();
	}

	return m_exitStatus;
}
