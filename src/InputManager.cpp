#include "InputManager.h"

#include "Engine.h"

#include "globals.h"

InputManager* g_inputManager;

void InputManager::PollEvents()
{
	SDL_Event event;
	while (SDL_PollEvent(&event))
	{
		// Generated from list of events.
		switch (event.type)
		{
			// case SDL_AUDIODEVICEADDED:
			// case SDL_AUDIODEVICEREMOVED:
			//     OnAudioDeviceEvent(event.adevice);
			//     break;
			// case SDL_CONTROLLERAXISMOTION:
			//     OnControllerAxisEvent(event.caxis);
			//     break;
			// case SDL_CONTROLLERBUTTONDOWN:
			// case SDL_CONTROLLERBUTTONUP:
			//     OnControllerButtonEvent(event.cbutton);
			//     break;
			// case SDL_CONTROLLERDEVICEADDED:
			// case SDL_CONTROLLERDEVICEREMOVED:
			// case SDL_CONTROLLERDEVICEREMAPPED:
			//     OnControllerDeviceEvent(event.cdevice);
			//     break;
			// case SDL_DOLLARGESTURE:
			// case SDL_DOLLARRECORD:
			//     OnDollarGestureEvent(event.dgesture);
			//     break;
			// case SDL_DROPFILE:
			// case SDL_DROPTEXT:
			// case SDL_DROPBEGIN:
			// case SDL_DROPCOMPLETE:
			//     OnDropEvent(event.drop);
			//     break;
			// case SDL_FINGERMOTION:
			// case SDL_FINGERDOWN:
			// case SDL_FINGERUP:
			//     OnTouchFingerEvent(event.tfinger);
			//     break;
			case SDL_KEYDOWN:
			case SDL_KEYUP:
				OnKeyboardEvent(event.key);
				break;
			// case SDL_JOYAXISMOTION:
			//     OnJoyAxisEvent(event.jaxis);
			//     break;
			// case SDL_JOYBALLMOTION:
			//     OnJoyBallEvent(event.jball);
			//     break;
			// case SDL_JOYHATMOTION:
			//     OnJoyHatEvent(event.jhat);
			//     break;
			// case SDL_JOYBUTTONDOWN:
			// case SDL_JOYBUTTONUP:
			//     OnJoyButtonEvent(event.jbutton);
			//     break;
			// case SDL_JOYDEVICEADDED:
			// case SDL_JOYDEVICEREMOVED:
			//     OnJoyDeviceEvent(event.jdevice);
			//     break;
			case SDL_MOUSEMOTION:
				OnMouseMotionEvent(event.motion);
				break;
			case SDL_MOUSEBUTTONDOWN:
			case SDL_MOUSEBUTTONUP:
				OnMouseButtonEvent(event.button);
				break;
			// case SDL_MOUSEWHEEL:
			//     OnMouseWheelEvent(event.wheel);
			//     break;
			// case SDL_MULTIGESTURE:
			//     OnMultiGestureEvent(event.mgesture);
			//     break;
			case SDL_QUIT:
				OnQuitEvent(event.quit);
				break;
			// case SDL_SYSWMEVENT:
			//     OnSysWMEvent(event.syswm);
			//     break;
			// case SDL_TEXTEDITING:
			//     OnTextEditingEvent(event.edit);
			//     break;
			// case SDL_TEXTINPUT:
			//     OnTextInputEvent(event.text);
			//     break;
			// case SDL_USEREVENT:
			//     OnUserEvent(event.user);
			//     break;
			case SDL_WINDOWEVENT:
				OnWindowEvent(event.window);
				break;
		}
	}
}

void InputManager::OnKeyboardEvent(const SDL_KeyboardEvent& KeyboardEvent)
{
	KeyboardEventDelegate.Call(KeyboardEvent);
	if (KeyboardEvent.keysym.sym == SDLK_ESCAPE) { g_window->SetMouseGrab(false); }
}

void InputManager::OnMouseMotionEvent(const SDL_MouseMotionEvent& MouseMotionEvent)
{
	MouseMotionEventDelegate.Call(MouseMotionEvent);
}

void InputManager::OnMouseButtonEvent(const SDL_MouseButtonEvent& MouseButtonEvent)
{
	MouseButtonEventDelegate.Call(MouseButtonEvent);
	g_window->SetMouseGrab(true);
}

void InputManager::OnQuitEvent(const SDL_QuitEvent& QuitEvent)
{
	QuitEventDelegate.Call(QuitEvent);
	g_engine->Quit();
}

void InputManager::OnWindowEvent(const SDL_WindowEvent& WindowEvent)
{
	WindowEventDelegate.Call(WindowEvent);
	switch (WindowEvent.event)
	{
		case SDL_WINDOWEVENT_RESIZED:
			// ResizeWindow(WindowEvent.data1, WindowEvent.data2);
			break;
	}
}
