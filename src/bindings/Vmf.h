#pragma once

#include "core/SharedObject.h"

SO_FUNC_TYPEDEF(foo, void, int);

class Vmf : public SharedObject
{
public:
	Vmf(const char* fileName) : SharedObject(fileName) { Load(); }

	virtual bool LoadFunctions() override
	{
		SO_LOAD_FN(foo, void, int);
		return true;
	}

	SO_HEADER_DEF(foo);
};

// auto p = SDL_LoadObject("libs/main.so");
// if (!p)
// {
// 	printf("No so!\n");
// 	return 1;
// }
// void (*f)(void) = (void (*)())SDL_LoadFunction(p, "foo");
// if (!f)
// {
// 	printf("No fn!\n");
// 	return 1;
// }
// f();
