#pragma once

#include <functional>

#include "util/delegates.h"

class Engine
{
private:
	/** Parameters, initialization. */
	Engine();

	~Engine();

	/** Whether the engine wants to run. If false, this means that SDL has quit. */
	bool m_isRunning = false;

	/** Exit status. Set by Quit(). */
	i32 m_exitStatus;

public:
	/** Initializes global engine. */
	static void Initialize();
	/** Prints versions of used libraries. */
	void PrintVersions() const;

	/** Deinitialize and quit application. */
	void Quit(i32 withExitStatus = 0);

	/** Timestep. */
	TDelegate<f32> OnUpdate;

	i32 BeginMainLoop(std::function<void(void)> loopFunction);
};

extern Engine* g_engine;
