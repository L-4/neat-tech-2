#pragma once

#include "util/ref.h"

class StringSlice
{
public:
	StringSlice() = default;
	StringSlice(const char* begin, u32 length) : m_begin(begin), m_length(length) {}

	const char* m_begin;
	u32 m_length;

	Ref<char> GetAsCString() const;

	bool operator==(const StringSlice& other) const;

	/** Make slice one char longer. */
	u32& operator++() { return ++m_length; }
};

class StringMarcher
{
public:
	u32 m_column;
	u32 m_row;

	StringMarcher(StringSlice string)
		: m_column(0),
		  m_row(0),
		  m_string(string),
		  m_index(0),
		  m_firstChar(true)
	{}

	StringSlice m_string;
	u32 m_index;
	/** @todo: This sucks in a major way. */
	bool m_firstChar;

	/** Gets the "current" char, and offsets the index by one. */
	char GetChar();
	/** Gets the current char + ofs. */
	char Peek(u32 offset) const;

	bool IsEof() const;

	void GoBack() { --m_index; }

	/** Creates a new string slice with the current char. */
	StringSlice BeginSlice() const;

	void PrintRowAndColumn(const char* prefix) const;
};
