#pragma once

#include "core/core.h"

#include "util/asserts.h"

#include <cstdlib>
#include <cstring>
#include <new>
#include <stdio.h>

// @todo: Deal with this warning. Issue is that we want to move the contents in the array in the
// case of a realloc, but the compiler doesn't know whether a type with a deleted move constructor
// depends on it's address staying the same.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wclass-memaccess"

template <typename T>
class Array
{
private:
	/** Number of real elements in array. */
	i32 m_count;
	/** Total capacity of array. */
	i32 m_capacity;
	/** Actual data. */
	T* m_data;

public:
	/** @todo: Better default initial allocation. */
	Array() : Array(8) {}
	Array(i32 initialAllocation)
		: m_count(0),
		  m_capacity(initialAllocation),
		  m_data((T*)malloc(sizeof(T) * initialAllocation))
	{}

	Array(const Array<T>& other)
		: m_count(other.m_count),
		  m_capacity(other.m_capacity),
		  m_data((T*)malloc(sizeof(T) * other.m_capacity))
	{
		std::memcpy(m_data, other.m_data, Sizeof());
	}

	~Array()
	{
		if (!m_data) return;
		for (i32 i = 0; i < m_count; ++i) m_data[i].~T();
		free(m_data);
		m_data = nullptr;
	}

	Array<T>& operator=(const Array<T>& other)
	{
		printf("Warning: Copying array!\n");
		ERROR("Copying of arrays is broken. GL!")

		// @todo: Needed?
		if (this != &other)
		{
			m_count	   = other.m_count;
			m_capacity = other.m_capacity;
			m_data	   = (T*)malloc(sizeof(T) * m_capacity);
		}

		return *this;
	}

	/** Range iterators. */
	T* begin() const { return m_data; }
	T* end() const { return m_data + m_count; }

	/** Ensure that we have enough capacity for m_count + extraCount. */
	void EnsureCapacity(i32 extraCount = 1)
	{
		if (m_capacity < m_count + extraCount) { Grow(m_capacity + extraCount); }
	}

	/** Returns number of existing elements. */
	i32 GetCount() const { return m_count; }

	void Clear()
	{
		for (i32 i = 0; i < m_count; ++i) { m_data[i].~T(); }

		m_count = 0;
	}

	/** Returns pointer to beginning of data. */
	T* Ptr() const { return m_data; }
	/**
	 * Returns pointer to first empty index.
	 * Might be invalid, so be sure to call EnsureCapacity() first!
	 */
	T* FirstEmptyPtr() const { return m_data + m_count; }

	/**
	 * Reserve `count * sizeof(T)` space, assume that it will be filled, and return a raw ptr to
	 * the start of the data.
	 */
	T* GetWritablePtr(i32 count)
	{
		EnsureCapacity(count);
		T* ptr = FirstEmptyPtr();

		m_count += count;
		return ptr;
	}

	/** Construct element in place. */
	template <typename... TArgs>
	T& Emplace(TArgs&&... args)
	{
		i32 newIndex = m_count++;
		EnsureCapacity();
		/** @todo: Forward. */
		new (m_data + newIndex) T(args...);

		return m_data[newIndex];
	}

	/** Get reference to last item in array. */
	T& Last() { return m_data[m_count - 1]; }

	/** Push instance of item. */
	void Push(const T& newItem)
	{
		EnsureCapacity();
		m_data[m_count] = newItem;
		++m_count;
	}

	void Pop()
	{
		if (m_count == 0) return;

		m_data[m_count - 1].~T();
		--m_count;
	}

	/** Initalize all uninitialized elements. */
	template <typename... TArgs>
	void InitializeUninitialized(TArgs&&... args)
	{
		for (i32 i = m_count; i < m_capacity; ++i) new (m_data + i) T(args...);

		m_count = m_capacity;
	}

	/** Returns byte length of array of existing elements. (Not size of the allocation.) */
	i32 Sizeof() const { return m_count * sizeof(T); }

	T& operator[](const i32 index)
	{
		ASSERT(index < m_count);
		return m_data[index];
	}

	const T& operator[](const i32 index) const
	{
		ASSERT(index < m_count);
		return m_data[index];
	}

	/** Get index of item. */
	i32 IndexOf(const T& item) const
	{
		for (i32 index = 0; index < m_count; ++index)
		{
			if (m_data[index] == item) { return index; }
		}

		return -1;
	}

	/**
	 * Remove item at index, and swap in last item.
	 * @cleanup Better name.
	 */
	void RemoveSwapWithLast(i32 index)
	{
		ASSERT(index >= 0);
		ASSERT(index < m_count);

		m_data[index].~T();
		// We only have to move stuff around if there's anything left in the array.
		if (m_count > 1) { memcpy(m_data + index, m_data + m_count - 1, sizeof(T)); }

		--m_count;
	}

	/** Reallocates to targetCapacity. Must be enough to store existing elements. */
	void Resize(const i32 targetCapacity)
	{
		if (targetCapacity == m_capacity) return;

		ASSERT(targetCapacity >= m_count);

		m_data = static_cast<T*>(realloc(m_data, sizeof(T) * targetCapacity));
		ASSERT(m_data);
		m_capacity = targetCapacity;
	}

#define TARRAY_GROW_POWERS_OF_TWO
	/** Like Resize(), but selects a 'better' amount to grow by. */
	void Grow(const i32 targetCapacity)
	{
		ASSERT(targetCapacity > m_capacity);

#ifdef TARRAY_GROW_POWERS_OF_TWO
		i32 newTargetCapacity = 1;
		while (newTargetCapacity < targetCapacity) newTargetCapacity <<= 1;
		Resize(newTargetCapacity);
#else
		Resize(targetCapacity);
#endif
	}
};
#pragma GCC diagnostic pop
