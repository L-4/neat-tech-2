#include "StringSlice.h"

#include "util/asserts.h"

#include <cstring>
#include <stdio.h>

Ref<char> StringSlice::GetAsCString() const
{
	ASSERT(m_begin);
	char* cString = new char[m_length + 1];
	memcpy(cString, m_begin, m_length);
	cString[m_length] = '\0';

	return Ref<char>(cString, true);
}

bool StringSlice::operator==(const StringSlice& other) const
{
	ASSERT(m_begin);
	if (m_length != other.m_length) { return false; }

	for (u32 i = 0; i < m_length; ++i)
	{
		if (m_begin[i] != other.m_begin[i]) { return false; }
	}

	return true;
}

char StringMarcher::GetChar()
{
	if (m_firstChar)
	{
		m_firstChar = false;
		return *(m_string.m_begin);
	}

	char c = *(m_string.m_begin + ++m_index);

	if (c == '\n')
	{
		++m_row;
		m_column = 0;
	}
	else
	{
		++m_column;
	}

	return c;
}

char StringMarcher::Peek(u32 offset) const
{
	return *(m_string.m_begin + m_index + offset);
}

bool StringMarcher::IsEof() const
{
	return m_index == m_string.m_length;
}

StringSlice StringMarcher::BeginSlice() const
{
	return StringSlice(m_string.m_begin + m_index, 1);
}

void StringMarcher::PrintRowAndColumn(const char* prefix) const
{
	printf("%s: %d:%d\n", prefix, m_row, m_column);
}
