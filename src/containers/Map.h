#pragma once

#include "Pair.h"

#include "util/asserts.h"

static const u32 TABLE_SIZE = 256;

namespace Hash
{

u32 Hash(u32 val)
{
	return val % TABLE_SIZE;
}

} // namespace Hash

template <typename K, typename V>
class Map
{
	struct Node
	{
		Node(const K& key, const V& value) : m_pair(key, value), m_next(nullptr) {}

		Pair<K, V> m_pair;
		Node* m_next;
	};

	Node** m_buckets;

public:
	Map() : m_buckets(new Node*[TABLE_SIZE]) {}
	~Map() { delete[] m_buckets; }

	void Set(const K& key, const V& value)
	{
		u32 hash = Hash::Hash(key);
		ASSERT(hash < TABLE_SIZE);
		Node* entry = m_buckets[hash];

		if (!entry)
		{
			entry = m_buckets[hash] = new Node(key, value);
			return;
		}
		else
		{
			while (entry->m_next) entry = entry->m_next;
			entry->m_next = new Node(key, value);
		}
	}

	const V& Get(const K& key) const
	{
		u32 hash = Hash::Hash(key);
		ASSERT(hash < TABLE_SIZE);
		Node* entry = m_buckets[hash];

		ASSERT_MSG(entry, "Could not get value for key.");

		while (entry->m_pair.m_key != key && entry->m_next) entry = entry->m_next;

		ASSERT(entry->m_pair.m_key == key);
		return entry->m_pair.m_value;
	}

	V* GetOrNull(const K& key) const
	{
		u32 hash = Hash::Hash(key);
		ASSERT(hash < TABLE_SIZE);
		Node* entry = m_buckets[hash];

		if (!entry) return nullptr;

		while (entry->m_pair.m_key != key)
		{
			if (!entry->m_next) return nullptr;
			entry = entry->m_next;
		}

		return &entry->m_pair.m_value;
	}
};
