#pragma once

template <typename K, typename V>
class Pair
{
public:
	K m_key;
	V m_value;

	Pair(K& key, V& value) : m_key(key), m_value(value) {}
	Pair(const K& key, const V& value) : m_key(key), m_value(value) {}
};
