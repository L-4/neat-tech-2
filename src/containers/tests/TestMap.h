#pragma once

#include "containers/Map.h"

void TestMap()
{
	printf("Beginning Map<K, V> tests...\n");
	Map<u32, const char*> map;

	// Set/get
	map.Set(12345, "hello world\n");
	ASSERT(map.Get(12345) == "hello world\n");

	// Get null
	ASSERT(map.GetOrNull(54321) == nullptr);

	// Get not null
	ASSERT(map.GetOrNull(12345) != nullptr);

	printf("Map<K, V> tests passed!\n");
}
