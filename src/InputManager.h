#pragma once

#include "SDL2/SDL_events.h"

#include "util/asserts.h"
#include "util/delegates.h"

// @todo: ????
class InputManager;
extern InputManager* g_inputManager;

class InputManager
{
public:
	static void Initialize()
	{
		ASSERT(!g_inputManager);
		g_inputManager = new InputManager();
	};

	void PollEvents();

private:
	// Mouse motion, mouse click, button events, quit

	// I definitely typed these out manually
	// void OnAudioDeviceEvent(const SDL_AudioDeviceEvent &adevice);
	// void OnControllerAxisEvent(const SDL_ControllerAxisEvent &caxis);
	// void OnControllerButtonEvent(const SDL_ControllerButtonEvent &cbutton);
	// void OnControllerDeviceEvent(const SDL_ControllerDeviceEvent &cdevice);
	// void OnDollarGestureEvent(const SDL_DollarGestureEvent &dgesture);
	// void OnDropEvent(const SDL_DropEvent &drop);
	// void OnTouchFingerEvent(const SDL_TouchFingerEvent &tfinger);
	void OnKeyboardEvent(const SDL_KeyboardEvent& key);
	// void OnJoyAxisEvent(const SDL_JoyAxisEvent &jaxis);
	// void OnJoyBallEvent(const SDL_JoyBallEvent &jball);
	// void OnJoyHatEvent(const SDL_JoyHatEvent &jhat);
	// void OnJoyButtonEvent(const SDL_JoyButtonEvent &jbutton);
	// void OnJoyDeviceEvent(const SDL_JoyDeviceEvent &jdevice);
	void OnMouseMotionEvent(const SDL_MouseMotionEvent& motion);
	void OnMouseButtonEvent(const SDL_MouseButtonEvent& button);
	// void OnMouseWheelEvent(const SDL_MouseWheelEvent &wheel);
	// void OnMultiGestureEvent(const SDL_MultiGestureEvent &mgesture);
	void OnQuitEvent(const SDL_QuitEvent& quit);
	// void OnSysWMEvent(const SDL_SysWMEvent &syswm);
	// void OnTextEditingEvent(const SDL_TextEditingEvent &edit);
	// void OnTextInputEvent(const SDL_TextInputEvent &text);
	// void OnUserEvent(const SDL_UserEvent &user);
	void OnWindowEvent(const SDL_WindowEvent& window);

public:
	// TDelegate<const SDL_AudioDeviceEvent &> AudioDeviceEventDelegate;
	// TDelegate<const SDL_ControllerAxisEvent &> ControllerAxisEventDelegate;
	// TDelegate<const SDL_ControllerButtonEvent &> ControllerButtonEventDelegate;
	// TDelegate<const SDL_ControllerDeviceEvent &> ControllerDeviceEventDelegate;
	// TDelegate<const SDL_DollarGestureEvent &> DollarGestureEventDelegate;
	// TDelegate<const SDL_DropEvent &> DropEventDelegate;
	// TDelegate<const SDL_TouchFingerEvent &> TouchFingerEventDelegate;
	TDelegate<const SDL_KeyboardEvent&> KeyboardEventDelegate;
	// TDelegate<const SDL_JoyAxisEvent &> JoyAxisEventDelegate;
	// TDelegate<const SDL_JoyBallEvent &> JoyBallEventDelegate;
	// TDelegate<const SDL_JoyHatEvent &> JoyHatEventDelegate;
	// TDelegate<const SDL_JoyButtonEvent &> JoyButtonEventDelegate;
	// TDelegate<const SDL_JoyDeviceEvent &> JoyDeviceEventDelegate;
	TDelegate<const SDL_MouseMotionEvent&> MouseMotionEventDelegate;
	TDelegate<const SDL_MouseButtonEvent&> MouseButtonEventDelegate;
	// TDelegate<const SDL_MouseWheelEvent &> MouseWheelEventDelegate;
	// TDelegate<const SDL_MultiGestureEvent &> MultiGestureEventDelegate;
	TDelegate<const SDL_QuitEvent&> QuitEventDelegate;
	// TDelegate<const SDL_SysWMEvent &> SysWMEventDelegate;
	// TDelegate<const SDL_TextEditingEvent &> TextEditingEventDelegate;
	// TDelegate<const SDL_TextInputEvent &> TextInputEventDelegate;
	// TDelegate<const SDL_UserEvent &> UserEventDelegate;
	TDelegate<const SDL_WindowEvent&> WindowEventDelegate;
};
