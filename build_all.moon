-- #!/usr/bin/env lua -- Not supported, it seems

import execute from os
import format from string

true_false = { "true", "false" }
compilers = { "g++", "clang++" }

build_permutations = [{compiler, debug, editor} for compiler in *compilers for debug in *true_false for editor in *true_false]

for build in *build_permutations
    execute format "make -j8 compiler=%s debug=%s editor=%s", unpack build
