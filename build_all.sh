#!/bin/bash

TF="true false"

for compiler in "clang++" "g++";
do
    for debug in $TF;
    do
        for editor in $TF;
        do
            make debug=$debug editor=$editor compiler=$compiler -j8 &>/dev/null
            echo "debug=$debug editor=$editor compiler=$compiler $?"
        done;
    done;
done
